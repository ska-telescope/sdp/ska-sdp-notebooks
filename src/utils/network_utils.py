import logging
import socket
import time
from typing import List

import ska_ser_logging

TIMEOUT = 60.0  # seconds
INTERVAL = 0.5  # seconds

logger = logging.getLogger(__name__)
ska_ser_logging.configure_logging(level=logging.INFO)


def wait_for_hosts(hosts: List[str], attempts=18, interval=5):
    # check all hosts are receiving
    for host in set(hosts):
        found = False
        for attempt in range(attempts):
            try:
                socket.gethostbyname(host.encode("ascii"))
                found = True
                break
            except socket.gaierror:
                logger.info(f"waiting for host {host}")
                time.sleep(interval)
        if not found:
            raise TimeoutError(host)
