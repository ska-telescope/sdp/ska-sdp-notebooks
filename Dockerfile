FROM artefact.skao.int/ska-build-python:0.1.1 AS build

WORKDIR /build
COPY . ./

ENV POETRY_VIRTUALENVS_CREATE=false

RUN poetry install --no-root --extras gsutil --only main

FROM artefact.skao.int/ska-python:0.1.2

# copy installed python packages
COPY --from=build /usr/local/ /usr/local/

# needed to download data from gitlab repositories
RUN apt-get update && apt-get install -y curl

RUN groupadd -g 1000 jovyan
RUN useradd jovyan -u 1000 -g 1000 -m -s /bin/bash

WORKDIR /app
COPY --chown=jovyan:jovyan ./src/ ./

RUN chown -R jovyan:jovyan /app

USER jovyan
