{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Global Pointing Observation Load Simulation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [pointing offset script](https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/tree/master/src/pointing-offset) deploys the [pointing offset calibration pipeline](https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/)\n",
    "which calculates the pointing offsets from \"pointing\"-type scan data.\n",
    "\n",
    "To generate a global pointing model, observations of many different sources will be made, consisting of multiple scans per observation. This notebook attempts to simulate the load the pointing pipeline will be under in this scenario, by running 50 observations of 5 scans on the same data.\n",
    "\n",
    "To deploy SDP, follow the instruction at [Installing SDP](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/standalone.html). You will need to deploy\n",
    "SDP version 0.24.0 on the Data Processing Platform (see [further instructions here](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/remote-cluster.html#data-processing-dp-cluster)\n",
    "SDP will need to be deployed with access to the `shared` or `shared-mnl` PVC. You can achieve this by setting the following helm value `global.data-product-pvc-name: shared`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up tango and data for processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    },
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Import all the required packages and define `namespace`, `databaseds` service and set the `TANGO_HOST`. These\n",
    "are needed to connect to the SDP system, and within that to a subarray device."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# IMPORTS\n",
    "\n",
    "import os\n",
    "import json\n",
    "import random\n",
    "import logging\n",
    "import ska_ser_logging\n",
    "import h5py\n",
    "\n",
    "from asyncio import sleep\n",
    "from datetime import date\n",
    "from tango import DeviceProxy, DevFailed\n",
    "\n",
    "from utils.generate_qc_exchanges import mock_dish_exchange\n",
    "from utils.pointing_script_utils import mock_pointings, cbf_scan\n",
    "from utils.subarray_utils import wait_for_obs_state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# configure ska logging, change as needed \n",
    "logger = logging.getLogger(__name__)\n",
    "ska_ser_logging.configure_logging(level=logging.INFO)\n",
    "\n",
    "# specify here the control system namespace to connect in this cluster\n",
    "KUBE_NAMESPACE = \"<update-with-ns!!!>\"\n",
    "\n",
    "# set the name of the databaseds service\n",
    "DATABASEDS_NAME = \"databaseds-tango-base\"\n",
    "\n",
    "# set the KAFKA_HOST\n",
    "KAFKA_HOST = f\"ska-sdp-kafka.{KUBE_NAMESPACE}:9092\"\n",
    "\n",
    "# set the TANGO_HOST\n",
    "os.environ[\"TANGO_HOST\"] = f\"{DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.cluster.local:10000\"\n",
    "\n",
    "# set connection to the Configuration Database\n",
    "os.environ[\"SDP_CONFIG_HOST\"] = f\"ska-sdp-etcd-client.{KUBE_NAMESPACE}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "The below cell creates three Kafka topics with custom properties. These are topics that the mswriter pod uses to read pointing data from. We update the retention on these pods so that the topics are cleared in a timely manner. Without this re-running the pipeline with the same data, as we do in this notebook, would result in duplicate entries in the MeasurementSets and break the pipeline. \n",
    "\n",
    "See also (and related pages): https://confluence.skatelescope.org/display/SE/PI24.4+Kafka+retaining+old+entries+-+investigation\n",
    "\n",
    "Note that SDP needs to be installed with the following kafka settings for the deletion of data to take effect quickly:\n",
    "\n",
    "    kafka:\n",
    "        logRetentionCheckIntervalMs: \"10000\"\n",
    "        \n",
    "This will reset the retention check interval to 10 seconds.\n",
    "\n",
    "[!IMPORTANT]\n",
    "Only run the following cell once per life of the Kafka pod!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "# ONLY RUN ONCE! \n",
    "# Only rerun this cell if you have restarted the kafka pod first.\n",
    " \n",
    "from kafka.admin import NewTopic, KafkaAdminClient\n",
    " \n",
    "# these are the topics that the mswriter reads the pointing data from;\n",
    "# we need to remove the data from these to prevent duplicate entries in the MS\n",
    "topics = [\"commanded-pointings\", \"actual-pointings\", \"source-offsets\"]\n",
    " \n",
    "# segment.ms: sets the segment retention time\n",
    "# retention.ms: sets the individual message retention time\n",
    "# retention.bytes: I read somewhere that this needs to be reset to -1 so that the retention time can take effect (it's the retention of logs in size)\n",
    "topic_config = {\"segment.ms\": 5000, \"retention.ms\": 5000, \"retention.bytes\": -1}\n",
    " \n",
    "new_topics = []\n",
    " \n",
    "admin = KafkaAdminClient(bootstrap_servers=KAFKA_HOST)\n",
    " \n",
    "for topic in topics:\n",
    "    new_topics.append(NewTopic(name=topic, num_partitions=1, replication_factor=1, topic_configs=topic_config))\n",
    " \n",
    "admin.create_topics(new_topics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "The following step is only needed, if a development version of the pointing-offset script is used!\n",
    "\n",
    "Note, that this will fail, if the version already exists in the Config DB. This failure will not break SDP or any of the following steps. Uncomment the content of the cell and update with the relevant image and script version (replace the placeholder `<>`s) as needed. If the version already exists, and you want to replace it, you can use `ska-sdp update script ...` to do that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "# Add the pointing processing script definition\n",
    "!ska-sdp create script realtime:pointing-offset:<> '{\"image\": \"<>\"}'\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to download the MeasurementSet (MS) data files that the pointing pipeline will process to the location where the CBF emulator can access them for sending over to vis-receive. In addition, we also need to provide the pointing data (commanded, actual pointings, and source offsets), which are stored in HDF files. These will be saved in the MS files during the receive process, and are essential for the pointing offset calibration pipeline to run.\n",
    "\n",
    "Both the MS data and the pointing data can be found in the [SDP Integration repository](https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/tree/master/tests/resources/data/pointing-data), in a tar file. The data are simulated 5 scans with the SKA AA0.5 configuration at Band 2. They have been\n",
    "reordered in ANTENNA and TIME columns to match the baseline order expected by the receiver (see README of the tar file).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "DATA_DIR = \"/shared/pointing_offset_tests/pointing_data\"\n",
    "HDF_DATA_DIR = f\"{DATA_DIR}/pointing-hdfs\"\n",
    "\n",
    "DATA_URL = \"https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/raw/master/tests/resources/data/pointing-data/pointing-data.tar\"\n",
    "\n",
    "if not os.path.isdir(DATA_DIR):\n",
    "    os.makedirs(DATA_DIR)\n",
    "    !cd $DATA_DIR\n",
    "    !curl -O $DATA_URL\n",
    "    !tar -xvf pointing-data.tar \n",
    "\n",
    "else:\n",
    "     print(f\"Data are already available in {DATA_DIR}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "MS_SCANS = [\n",
    "    \"scan-1.ms\", \n",
    "    \"scan-2.ms\", \n",
    "    \"scan-3.ms\", \n",
    "    \"scan-4.ms\", \n",
    "    \"scan-5.ms\"\n",
    "]\n",
    "\n",
    "ACTUAL_HDF_SCANS = [\n",
    "    \"actual_pointing_scan-1.hdf\", \n",
    "    \"actual_pointing_scan-2.hdf\", \n",
    "    \"actual_pointing_scan-3.hdf\", \n",
    "    \"actual_pointing_scan-4.hdf\", \n",
    "    \"actual_pointing_scan-5.hdf\"\n",
    "]\n",
    "\n",
    "REQUESTED_HDF_SCANS = [\n",
    "    \"requested_pointing_scan-1.hdf\", \n",
    "    \"requested_pointing_scan-2.hdf\", \n",
    "    \"requested_pointing_scan-3.hdf\", \n",
    "    \"requested_pointing_scan-4.hdf\", \n",
    "    \"requested_pointing_scan-5.hdf\"\n",
    "]\n",
    "\n",
    "SOURCE_OFFSET_HDF_SCANS = [\n",
    "    \"source_offset_scan-1.hdf\", \n",
    "    \"source_offset_scan-2.hdf\", \n",
    "    \"source_offset_scan-3.hdf\", \n",
    "    \"source_offset_scan-4.hdf\", \n",
    "    \"source_offset_scan-5.hdf\"\n",
    "]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "## AssignResources, Configure, and Scan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Let's start by obtaining a handle of the subarray tango device, then check if the device is ON and the obstate is `EMPTY`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "subarray_device = DeviceProxy(\"test-sdp/subarray/01\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    subarray_device.On()\n",
    "except DevFailed:\n",
    "    print(\"The device is already in ON state!\")\n",
    "\n",
    "subarray_device.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "The next step is to set up the configuration string. The following example will deploy one processing block for pointing-offset and another for vis-receive. In the following JSON string, update the image and version for pointing-offset as needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "generator = \"multiobs\"\n",
    "today = date.today().strftime(\"%Y%m%d\")\n",
    "number = random.randint(0, 99998)\n",
    "\n",
    "EXECUTION_BLOCK_ID = f\"eb-{generator}-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_REALTIME = f\"pb-{generator}vr-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_POINTING = f\"pb-{generator}po-{today}-{number:05d}\"\n",
    "\n",
    "DISH_IDS = range(4)\n",
    "RECEPTORS = [ \"SKA001\", \"SKA036\", \"SKA063\", \"SKA100\" ]\n",
    "TOTAL_CHANNELS = 8  # should match emulator data\n",
    "\n",
    "exchanges = mock_dish_exchange(DISH_IDS, KAFKA_HOST)\n",
    "\n",
    "config = {\n",
    "    \"interface\": \"https://schema.skao.int/ska-sdp-assignres/0.4\",\n",
    "    \"resources\": {\n",
    "        \"receptors\": RECEPTORS,\n",
    "        \"receive_nodes\": 1,\n",
    "    },\n",
    "    \"execution_block\": {\n",
    "        \"eb_id\": EXECUTION_BLOCK_ID,\n",
    "        \"context\": {},\n",
    "        \"max_length\": 21600.0,\n",
    "        \"channels\": [\n",
    "            {\n",
    "                \"channels_id\": \"vis_channels\",\n",
    "                 \"spectral_windows\": [{\n",
    "                     \"spectral_window_id\": \"fsp_1_channels\",\n",
    "                     \"count\": TOTAL_CHANNELS,\n",
    "                     \"start\": 0,\n",
    "                     \"stride\": 1,\n",
    "                     \"freq_min\": 1.2925e9,\n",
    "                     \"freq_max\": 1.4125e9,\n",
    "                     \"link_map\": [ [0, 0], [200, 1], [744, 2], [944, 3] ]\n",
    "                    }]\n",
    "            }\n",
    "        ],\n",
    "        \"polarisations\": [\n",
    "            {\n",
    "                \"polarisations_id\": \"all\",\n",
    "                \"corr_type\": [\"XX\", \"XY\", \"YX\", \"YY\"],\n",
    "            }\n",
    "        ],\n",
    "        \"fields\": [\n",
    "            {\n",
    "                \"field_id\": \"field_b\",\n",
    "                \"phase_dir\": {\n",
    "                    \"ra\": [294.8629],\n",
    "                    \"dec\": [-63.44029],\n",
    "                    \"reference_time\": \"...\",\n",
    "                    \"reference_frame\": \"ICRF3\",\n",
    "                },\n",
    "                \"pointing_fqdn\": \"low-tmc/telstate/0/pointing\",\n",
    "            },\n",
    "        ],\n",
    "        \"beams\": [{\"beam_id\": \"vis0\", \"function\": \"visibilities\"}],\n",
    "        \"scan_types\": [\n",
    "            {\n",
    "                \"scan_type_id\": \".default\",\n",
    "                \"beams\": {\n",
    "                    \"vis0\": {\n",
    "                        \"polarisations_id\": \"all\",\n",
    "                        \"channels_id\": \"vis_channels\",\n",
    "                    }\n",
    "                },\n",
    "            },\n",
    "            {\n",
    "                \"scan_type_id\": \"pointing\",\n",
    "                \"derive_from\": \".default\",\n",
    "                \"beams\": {\"vis0\": {\"field_id\": \"field_b\"}},\n",
    "            },\n",
    "        ],\n",
    "    },\n",
    "    \"processing_blocks\": [\n",
    "        {\n",
    "            \"pb_id\": PROCESSING_BLOCK_ID_POINTING,\n",
    "             \"script\":{\n",
    "                \"kind\":\"realtime\",\n",
    "                \"name\":\"pointing-offset\",\n",
    "                \"version\":\"0.8.0\"\n",
    "             },\n",
    "             \"parameters\": {\n",
    "        \t\t\"version\": \"0.9.0\",\n",
    "                \"num_scans\": 5,\n",
    "                \"additional_args\": [\"--use_source_offset_column\", \n",
    "                                    \"--use_modelvis\",\n",
    "                                    \"--num_chunks\",  \"8\",\n",
    "                                    \"--bw_factor\", \"0.95\", \"0.95\"]\n",
    "             },\n",
    "             \"dependencies\": [\n",
    "                 {\n",
    "                     \"pb_id\": PROCESSING_BLOCK_ID_REALTIME, \"kind\": [\"vis-receive\"]\n",
    "                 }\n",
    "             ],\n",
    "        },\n",
    "        {\n",
    "            \"pb_id\": PROCESSING_BLOCK_ID_REALTIME,\n",
    "            \"script\": {\n",
    "                \"kind\": \"realtime\",\n",
    "                \"name\": \"vis-receive\",\n",
    "                \"version\": \"4.5.0\",\n",
    "            },\n",
    "            \"parameters\": {\n",
    "                \"channels_per_port\": TOTAL_CHANNELS,\n",
    "                \"queue_connector_configuration\": {\n",
    "                    \"exchanges\": exchanges\n",
    "                },\n",
    "                \"transport_protocol\": \"tcp\",\n",
    "                \"extra_helm_values\": {\n",
    "                    \"receiver\": {\n",
    "                        \"options\": {\n",
    "                           \"reception\": {\n",
    "                               \"reset_time_indexing_after_each_scan\": True\n",
    "                           },\n",
    "                           \"telescope_model\": {\n",
    "                               \"telmodel_key\": \"instrument/ska1_mid/layout/mid-layout.json\"\n",
    "                           }\n",
    "                        }\n",
    "                    },\n",
    "                },\n",
    "                \"pod_settings\": [\n",
    "                    {\"securityContext\": {\"runAsUser\": 0, \"fsGroup\": 0}}\n",
    "                ],\n",
    "                \"processors\": {\n",
    "                    \"mswriter\": {\n",
    "                        \"args\": [\n",
    "                            \"--readiness-file=/tmp/processor_ready\",\n",
    "                            \"--commanded-pointing-topic=commanded-pointings\",\n",
    "                            \"--actual-pointing-topic=actual-pointings\",\n",
    "                            \"--source-offset-topic=source-offsets\",\n",
    "                            \"output.ms\",\n",
    "                        ],\n",
    "                    }\n",
    "                }\n",
    "            },\n",
    "        }\n",
    "    ],\n",
    "}\n",
    "\n",
    "config_eb = json.dumps(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Let's execute the AssignResources command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "subarray_device.assignResources(config_eb)\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.IDLE, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "The AssignResources command will start a processing block for pointing-offset and one for vis-receive. These will trigger the deployment of\n",
    "two execution engines (one for the pointing offset calibration pipeline and another for the visibility receiver with MS writer)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Now, execute the Configure command by setting the scan_type to \"pointing\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "SCAN_TYPE_ID = \"pointing\"\n",
    "\n",
    "subarray_device.Configure(json.dumps(\n",
    "{\n",
    "    \"interface\": \"https://schema.skao.int/ska-sdp-configure/0.4\",\n",
    "    \"scan_type\": SCAN_TYPE_ID\n",
    "}\n",
    "))\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.READY, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "This will configure a pointing scan, which will trigger the pipeline to move to the next step, which is to wait for the right number of pointing scans to be executed. You can follow the progress of the pipeline via its logs.\n",
    "For this you will need KUBECONFIG access on your local machine. Then you can use `kubectl logs` or `k9s` to check the logs of the pod, which is running in the processing namespace (e.g. f\"{KUBE_NAMESPACE}-p\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "source": [
    "Next begin the 50 observations of 5 scans. For each observations the pipeline will start sending pointing data to Kafka queues, which the QueueConnector will forward to the `mswriter`. It also calls the CBF emulator to send the MS data. The `mswriter` (part of `receive`) takes the data sent by the emulator and Kafka and writes them into a MeasurementSet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "NUM_OBS = 50\n",
    "SCANS_PER_OBS = 5\n",
    "TOTAL_SCANS = NUM_OBS * SCANS_PER_OBS\n",
    "\n",
    "MS_DATA = {\n",
    "    scan_id + 1: f\"{DATA_DIR}/{MS_SCANS[scan_id % SCANS_PER_OBS]}\"\n",
    "    for scan_id in range(TOTAL_SCANS)\n",
    "}\n",
    "# Get Receive Addresses\n",
    "receiveAddresses = json.loads(subarray_device.receiveAddresses)\n",
    "\n",
    "# only send to 1st beam address\n",
    "beam_id = list(receiveAddresses[SCAN_TYPE_ID].keys())[0]\n",
    "host = receiveAddresses[SCAN_TYPE_ID][beam_id][\"host\"][0][1]\n",
    "start_port = receiveAddresses[SCAN_TYPE_ID][beam_id][\"port\"][0][1]\n",
    "\n",
    "for obs_num in range(NUM_OBS):\n",
    "    print(f\"\\nStarting Observation {obs_num + 1}/{NUM_OBS}\")\n",
    "    for scan_num in range(SCANS_PER_OBS):\n",
    "        scan_id = obs_num * SCANS_PER_OBS + scan_num + 1\n",
    "        data_index = scan_num\n",
    "\n",
    "        act_data_file = f\"{HDF_DATA_DIR}/{ACTUAL_HDF_SCANS[data_index]}\"\n",
    "        comm_data_file = f\"{HDF_DATA_DIR}/{REQUESTED_HDF_SCANS[data_index]}\"\n",
    "        source_offset_data_file = f\"{HDF_DATA_DIR}/{SOURCE_OFFSET_HDF_SCANS[data_index]}\"\n",
    "    \n",
    "    \n",
    "        print(\">> Scan %i\" % scan_id)\n",
    "        subarray_device.Scan(\n",
    "            json.dumps(\n",
    "                {\n",
    "                    \"interface\": \"https://schema.skao.int/ska-sdp-scan/0.4\",\n",
    "                    \"scan_id\": scan_id,\n",
    "                }\n",
    "            )\n",
    "        )\n",
    "        wait_for_obs_state(subarray_device, subarray_device.obsState.SCANNING)\n",
    "\n",
    "        print(\"Sending pointing data...\")\n",
    "        await mock_pointings(DISH_IDS, act_data_file, \"achieved_pointing\", KAFKA_HOST)\n",
    "        await sleep(1)\n",
    "        await mock_pointings(DISH_IDS, comm_data_file, \"desired_pointing\", KAFKA_HOST)\n",
    "        await sleep(1)\n",
    "        await mock_pointings(DISH_IDS, source_offset_data_file, \"source_offset\", KAFKA_HOST)\n",
    "        await sleep(1)\n",
    "\n",
    "        print(\"Emulating scan...\")\n",
    "        await cbf_scan(MS_DATA[scan_id], host, start_port, scan_id)\n",
    "        await sleep(2)  # to give time for receiver to be ready again \n",
    "\n",
    "        print(\">> End Scan\")\n",
    "        subarray_device.EndScan()\n",
    "        wait_for_obs_state(subarray_device, subarray_device.obsState.READY)\n",
    "        \n",
    "    print(f\"Completed Observation {obs_num + 1}/{NUM_OBS}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After each observation, the pipeline should process the 5 MS that we saved in the $MS_DATA_DIR directory. You can monitor its progress via logs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing the results from the Data Processing Dashboard"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# IMPORTS\n",
    "\n",
    "import h5py\n",
    "import yaml\n",
    "\n",
    "from glob import glob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List the contents of the configuration database. There should be one `eb`. Copy the eb into the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ska-sdp list -a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "execution_block_id = \"eb-orcatest-20240814-94773\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specify the point at which the data PVC is mounted. If running this notebook from binderhub on the DP platform, the mount point will be `/shared`, or `/shared_mnl` for use with the Data Product Dashboard. Otherwise it could be `/mnt/data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pvc_mount = \"/shared\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each Execution Block for a pointing observation contains several processing blocks. The relevant \"pointing-offset\" processing blocks can be identified by examining the ```ska-data-product.yaml``` file that is included in each processing block product directory. For \"pointing-offset\" processing blocks, this yaml file lists the output HDF5 files containing the pointing offset data products."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "print(f\"\\nPointing output files for {execution_block_id}:\\n\")\n",
    "\n",
    "# Each EB has a directory on the shared storage\n",
    "directory_to_eb = f\"{pvc_mount}/product/{execution_block_id}/ska-sdp\"\n",
    "\n",
    "# Processing blocks within the EB are contained in separate directories\n",
    "# The pointing processing blocks are defined by the data product yaml file.\n",
    "product_yaml_files = glob(f\"{directory_to_eb}/**/ska-data-product.yaml\", recursive=True)\n",
    "\n",
    "# Extract the pointing result file locations from 'pointing-offset' processing blocks\n",
    "pointing_files = []\n",
    "for yaml_file in product_yaml_files:\n",
    "    with open(yaml_file, \"r\", encoding=\"utf8\") as file:\n",
    "            data = yaml.safe_load(file)\n",
    "    if data[\"config\"][\"processing_script\"] == \"pointing-offset\":\n",
    "        for pointing_file in data[\"files\"]:\n",
    "            pointing_files.append(pointing_file)\n",
    "            print(f\"   {pointing_file['description']}:\")\n",
    "            print(f\"   {pointing_file['path']}\")\n",
    "\n",
    "if pointing_files:\n",
    "    print(f\"\\nNumber of hdf5 files: {len(pointing_files)}\")\n",
    "else:\n",
    "    print(\"   None found\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read pointing output files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pointing output files are in HDF5 format, and can be read using the Python ```h5py``` library.\n",
    "\n",
    "Note that the paths in the data product yaml file are specified without the mount point, so this must be added to obtain the full file path.\n",
    "\n",
    "We will read the first and last of the `NUM_OBS` number of hdf5 files produced. Note that these wont necessarily be the first and last files that were produced, but the first and last in the `pointing_files` list generated above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "first_file = f\"{pvc_mount}{pointing_files[0]['path']}\"\n",
    "last_file = f\"{pvc_mount}{pointing_files[-1]['path']}\"\n",
    "\n",
    "files = [first_file, last_file]\n",
    "\n",
    "files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, open the files to find and compate the pointing offsets and other parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for file in files:\n",
    "    # Open the HDF5 file and list keys\n",
    "    pointing = h5py.File(file,\"r\")\n",
    "    pointing_keys = list(pointing.keys())\n",
    "\n",
    "    # List the attributes\n",
    "    print(f\"\\nAttributes for file: {file}\")\n",
    "    for attr in pointing[pointing_keys[0]].attrs.keys():\n",
    "        print(f\"{attr}:   {pointing[pointing_keys[0]].attrs[attr]}\")\n",
    "\n",
    "for file in files:\n",
    "    pointing = h5py.File(file,\"r\")\n",
    "    pointing_keys = list(pointing.keys())\n",
    "    \n",
    "    # Access the pointing table\n",
    "    print(f\"\\nPointing table and offsets for file: {file}\")\n",
    "    print(pointing[pointing_keys[0]].keys())\n",
    "\n",
    "    # Pointing offsets\n",
    "    print(pointing[pointing_keys[0]][\"data_pointing\"][()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clean up"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, end the processing and return the subarray to an EMPTY obsState"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "subarray_device.end()  # end the process (even if obsState is IDLE)\n",
    "subarray_device.releaseAllResourceS()  # release resources and get obsState back to EMPTY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "All the processing blocks should finish at this point"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, clean up the data directory for this execution block"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm -r $PVC_MOUNT/product/$EXECUTION_BLOCK_ID"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
