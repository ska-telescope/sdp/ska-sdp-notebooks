{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "eef726b7-7a0b-45fa-92e4-c704cda01d40",
   "metadata": {},
   "source": [
    "# **Running the test mock data processing script**\n",
    "\n",
    "Last updated: 24-Feb-2025"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44567496-6359-4e51-8975-62aaeac80e91",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "The [test mock data processing script](https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/tree/master/src/ska-sdp-script-test-mock-data) is a test script that is aimed to mock execution engines that include a variety of pipelines without actually running them in SDP. It supports a list of use scenarios, you can check the [Documentation](https://developer.skao.int/projects/ska-sdp-script/en/latest/test-scripts/test-mock-data.html) for information.\n",
    "\n",
    "This notebook provides an example that runs the \\\"pointing\\\" scenario of the script. It imitates a global pointing observation where a pointing HDF5 file is generated for each source observed. In this scenario, a set of pointing calibrators are observed across the observable sky. For each source, a five-point scan is performed. The script will deploy one processing block and then write data products depicting a full global pointing observation, by extracting offsets for the user-defined receptors from the template CSV file, and antenna configuration from the template HDF5 file. In the meantime, the extracted mock pointing offsets will be sent via Kafka to the QueueConnector device to be displayed in its tango attributes.\n",
    "\n",
    "To run the notebook, you need to deploy SDP in the designated namespace. This can be done via following the instruction at [Installing SDP](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/standalone.html). You will need to deploy on the Data Processing Platform (see [further instructions here](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/remote-cluster.html#data-processing-dp-cluster)).\n",
    "\n",
    "SDP will need to be deployed with access to the shared PVC. You can achieve this by setting the following helm value `global.data-product-pvc-name: shared`. \n",
    "\n",
    "This notebook works with SDP 1.0.0 and mock-data script 1.0.0."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b8ba2ecc484f82c",
   "metadata": {},
   "source": [
    "### Setting up tango and data for processing\n",
    "\n",
    "\n",
    "Import all the required packages and define namespace, databaseds service and set the TANGO_HOST. This will allow the notebook to connect to the tango devices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "504e4572-3098-4103-832d-74e221f58a90",
   "metadata": {},
   "outputs": [],
   "source": [
    "# IMPORTS\n",
    "\n",
    "import os\n",
    "import json\n",
    "import random\n",
    "import logging\n",
    "import ska_ser_logging\n",
    "\n",
    "from asyncio import sleep\n",
    "from datetime import date\n",
    "\n",
    "from tango import DeviceProxy, DevFailed\n",
    "from utils.subarray_utils import wait_for_obs_state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c7e1e7a-ce6c-4ce9-961c-2e545d94925c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# configure ska logging, change as needed \n",
    "logger = logging.getLogger(__name__)\n",
    "ska_ser_logging.configure_logging(level=logging.INFO)\n",
    "\n",
    "# specify here the control system namespace to connect in this cluster\n",
    "KUBE_NAMESPACE = \"<update-with-ns!!!>\"\n",
    "\n",
    "# set the name of the databaseds service\n",
    "DATABASEDS_NAME = \"databaseds-tango-base\"\n",
    "\n",
    "# set the KAFKA_HOST\n",
    "KAFKA_HOST = f\"ska-sdp-kafka.{KUBE_NAMESPACE}:9092\"\n",
    "\n",
    "# set the TANGO_HOST\n",
    "os.environ[\"TANGO_HOST\"] = f\"{DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.cluster.local:10000\"\n",
    "\n",
    "# set connection to the Configuration Database\n",
    "os.environ[\"SDP_CONFIG_HOST\"] = f\"ska-sdp-etcd-client.{KUBE_NAMESPACE}\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9fcf4416da5df8d2",
   "metadata": {},
   "source": [
    "### Use the right version of the test mock data\n",
    "\n",
    "(Optional) Create the test mock data pointing processing script if you need a custom one built from GitLab. This is not necessary if you use the default version in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "379591dd-2def-431a-a568-8e39c175c8d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "#!ska-sdp create script realtime:test-mock-data:1.0.0 '{\"image\": \"registry.gitlab.com/ska-telescope/sdp/ska-sdp-script/ska-sdp-script-test-mock-data:0.1.0-dev.cab650f71\"}'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee9b77edd5afa5f9",
   "metadata": {},
   "source": [
    " ### AssignResources, Configure, and Scan\n",
    "\n",
    " Let's start by obtaining a handle of the subarray tango device, then check if the device is ON and the obstate is EMPTY"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8081e89e-b882-4404-b278-0f4531290f53",
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray_device = DeviceProxy(\"test-sdp/subarray/01\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7303b576-b159-46a0-9d9e-77034a0d4507",
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    subarray_device.On()\n",
    "except DevFailed:\n",
    "    print(\"The device is already in ON state!\")\n",
    "\n",
    "subarray_device.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "501acb754985a000",
   "metadata": {},
   "source": [
    "The next step is to set up the configuration string. The following example will deploy one processing block and then write data products depicting a full global pointing observation, by extracting offsets for the user-defined receptors from the template CSV file, and antenna configuration from the template HDF5 file. Before running:\n",
    "\n",
    "1. check the version for the test mock data script\n",
    "2. check the processing script parameters: scenario and scenario specific parameters\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4375f0d-b497-4105-8ccb-22a28682781c",
   "metadata": {},
   "outputs": [],
   "source": [
    "generator = \"testmockdata\"\n",
    "today = date.today().strftime(\"%Y%m%d\")\n",
    "number = random.randint(0, 99998)\n",
    "\n",
    "EXECUTION_BLOCK_ID = f\"eb-{generator}-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_REALTIME = f\"pb-{generator}tmd-{today}-{number:05d}\"\n",
    "\n",
    "RECEPTORS = [ \"SKA001\", \"SKA036\", \"SKA063\", \"SKA100\" ]\n",
    "\n",
    "\n",
    "config = {\n",
    "  \"interface\": \"https://schema.skao.int/ska-sdp-assignres/1.0\",\n",
    "  \"resources\": {\n",
    "    \"receptors\": RECEPTORS\n",
    "  },\n",
    "  \"execution_block\": {\n",
    "    \"eb_id\": EXECUTION_BLOCK_ID,\n",
    "    \"context\": {},\n",
    "    \"max_length\": 3600.0,\n",
    "    \"beams\": [\n",
    "      {\"beam_id\": \"vis0\", \"function\": \"visibilities\"}\n",
    "    ],\n",
    "    \"scan_types\": [\n",
    "        {\n",
    "        \"scan_type_id\": \".default\",\n",
    "        \"beams\": {\n",
    "                \"vis0\": {\n",
    "                \"polarisations_id\": \"all\",\n",
    "                \"channels_id\": \"vis_channels\"\n",
    "            }\n",
    "        }\n",
    "    }, \n",
    "      {\n",
    "        \"scan_type_id\": \"pointing\",\n",
    "        \"derive_from\": \".default\",\n",
    "        \"beams\": {\"vis0\": {\"field_id\": \"field_b\"}},\n",
    "      },\n",
    "    ],\n",
    "    \"channels\": [\n",
    "      {\n",
    "        \"channels_id\": \"vis_channels\",\n",
    "        \"spectral_windows\": [\n",
    "          {\n",
    "            \"spectral_window_id\":\"fsp_1_channels\",\n",
    "            \"count\":4,\n",
    "            \"start\":0,\n",
    "            \"stride\":2,\n",
    "            \"freq_min\":350000000.0,\n",
    "            \"freq_max\":368000000.0,\n",
    "            \"link_map\":[[0, 0], [200, 1], [744, 2], [944, 3]]\n",
    "          }\n",
    "        ]\n",
    "      }\n",
    "    ],\n",
    "    \"polarisations\": [\n",
    "      {\n",
    "        \"polarisations_id\": \"all\",\n",
    "        \"corr_type\": [\"XX\", \"XY\", \"YX\", \"YY\"]\n",
    "      }\n",
    "    ],\n",
    "    \"fields\": [\n",
    "      {\n",
    "        \"field_id\": \"field_b\",\n",
    "        \"phase_dir\": {\n",
    "          \"target_name\": \"target_b\",\n",
    "          \"reference_frame\": \"icrs\",\n",
    "          \"attrs\": {\n",
    "            \"c1\": 123.0,\n",
    "            \"c2\": -60.0,\n",
    "            \"epoch\": 2000.0\n",
    "          }\n",
    "        }\n",
    "      }\n",
    "    ]\n",
    "  },\n",
    "  \"processing_blocks\": [\n",
    "    {\n",
    "      \"pb_id\": PROCESSING_BLOCK_ID_REALTIME,\n",
    "      \"script\": {\n",
    "      \"kind\": \"realtime\", \n",
    "      \"name\": \"test-mock-data\",\n",
    "      \"version\": \"1.0.0\"},\n",
    "      \"parameters\": {\n",
    "        \"scenario\": \"pointing\",\n",
    "        \"pointing_option\" : \"both\",\n",
    "      }\n",
    "    }\n",
    "  ]\n",
    "}\n",
    "\n",
    "config_eb = json.dumps(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0ff5eca0f491cd",
   "metadata": {},
   "source": [
    " Let's execute the AssignResources command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3e4b54b-6be3-402f-affa-30fdd205ce99",
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray_device.assignResources(config_eb)\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.IDLE, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "478a20bce4b3c986",
   "metadata": {},
   "source": [
    "Now, execute the Configure command by setting the scan_type to \"pointing\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0f6556b",
   "metadata": {},
   "outputs": [],
   "source": [
    "SCAN_TYPE_ID = \"pointing\"\n",
    "\n",
    "subarray_device.Configure(json.dumps(\n",
    "{\n",
    "    \"interface\": \"https://schema.skao.int/ska-sdp-configure/1.0\",\n",
    "    \"scan_type\": SCAN_TYPE_ID\n",
    "}\n",
    "))\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.READY, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf5476d44d7e9acd",
   "metadata": {},
   "source": [
    "Next, execute Scans. The following code will imitate a global pointing observation where a pointing HDF5 file will be generated for each source observed. In this scenario, up to 50 pointing calibrators\n",
    "can be observed across the observable sky, and for each source, a five-point scan is performed. However here, just for demonstration purposes, we only look at a one source, that corresponds to 5 scans."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e868b72c8fd4d504",
   "metadata": {},
   "outputs": [],
   "source": [
    "NUM_SCANS_PER_SOURCE = 5\n",
    "# Change the below to up to 50 sources\n",
    "NUM_SOURCES = 1\n",
    "for scan_id in range(NUM_SOURCES * NUM_SCANS_PER_SOURCE):\n",
    "    print(\">> Scan %i\" % (scan_id+1))\n",
    "    subarray_device.Scan(\n",
    "    json.dumps(\n",
    "        {\n",
    "            \"interface\": \"https://schema.skao.int/ska-sdp-scan/1.0\",\n",
    "            \"scan_id\": scan_id+1,\n",
    "        }\n",
    "    ))\n",
    "\n",
    "    wait_for_obs_state(subarray_device, subarray_device.obsState.SCANNING)\n",
    "\n",
    "    print(\">> End Scan\")\n",
    "    subarray_device.EndScan()\n",
    "    wait_for_obs_state(subarray_device, subarray_device.obsState.READY)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "079d207a-2cfe-4b0d-8d13-c17b127b0847",
   "metadata": {},
   "source": [
    "We can then check the data sent to QueueConnector. On the QueueConnector, each dish has its own tango attribute, here called `pointing_offset_<dish_name>`, for example `\"pointing_offset_SKA001\"`. \n",
    "\n",
    "The current one shows the latest pointing offset data that is being sent to a particular attribute (the last source). \n",
    "Please note that it may take some time (can be up to a minute - the reason is not yet known) for the data to appear on the QueueConnector, please refresh the cell below a few times and check the log of the processing block to make sure the data has been sent successfully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f5a14b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "q = DeviceProxy(\"test-sdp/queueconnector/01\")\n",
    "for dish_name in RECEPTORS:\n",
    "    print(\"%s: %s\" % (dish_name, q.read_attribute(f\"pointing_offset_{dish_name}\").value))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "868ac31e-e486-4abc-a254-3ed817678f72",
   "metadata": {},
   "source": [
    "### Check if the dataproducts exist\n",
    "\n",
    "Check the processing block directory for available scans; and look into one of the scan directories for the data products. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aba39b78-08e3-4565-8315-2c8dcc7a5097",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -lrth /shared/product/$EXECUTION_BLOCK_ID/ska-sdp/$PROCESSING_BLOCK_ID_REALTIME/\n",
    "!ls -lrth /shared/product/$EXECUTION_BLOCK_ID/ska-sdp/$PROCESSING_BLOCK_ID_REALTIME/scan1-5/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0bfb79ca-c8ce-4b7a-84de-7da9b816498e",
   "metadata": {},
   "source": [
    "### Clean up\n",
    "\n",
    "End the processing and return the subarray to an EMPTY obsState"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "667d57b7-0dd3-4cf8-9aa4-20e7dbe53c81",
   "metadata": {},
   "outputs": [],
   "source": [
    "subarray_device.end()  # end the process (even if obsState is IDLE)\n",
    "subarray_device.ReleaseAllResources() # release resources and get obsState back to EMPTY"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71dc5f6b-7948-4131-8ce8-8c228631da25",
   "metadata": {},
   "source": [
    "Clean up the data directory for this execution block (here we assume PVC is mounted onto shared, change to the correct pvc mount)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "760a1242-a325-4b84-a738-68b2c1c5de8a",
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm -rf /shared/product/$EXECUTION_BLOCK_ID"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37e8cf3a-220a-4038-8872-a7e8c754869e",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
