from realtime.receive.core.msutils import MeasurementSet


def calculate_ms_scan_model(measurement_set: MeasurementSet) -> dict:
    scan_type = measurement_set.calculate_scan_type()
    return {
        "scan_types": [
            {
                "scan_type_id": scan_type.scan_type_id,
                "beams": [
                    {
                        beam.beam_id: {
                            "field_id": beam.field.field_id,
                            "channels_id": beam.channels.channels_id,
                            "polarisations_id": beam.polarisations.polarisation_id,
                        }
                    }
                    for beam in scan_type.beams
                ],
            }
        ],
        "beams": [
            {"beam_id": beam.beam_id, "function": beam.function}
            for beam in scan_type.beams
        ],
        "channels": [
            {
                "channels_id": beam.channels.channels_id,
                "spectral_windows": [
                    {
                        "spectral_window_id": spectral_window.spectral_window_id,
                        "count": spectral_window.count,
                        "start": spectral_window.start,
                        "stride": spectral_window.stride,
                        "freq_min": spectral_window.freq_min,
                        "freq_max": spectral_window.freq_max,
                    }
                    for spectral_window in beam.channels.spectral_windows
                ],
            }
            for beam in scan_type.beams
        ],
        "polarisations": [
            {
                "polarisation_id": beam.polarisations.polarisation_id,
                "corr_type": [
                    stoke.name for stoke in beam.polarisations.correlation_type
                ],
            }
            for beam in scan_type.beams
        ],
        "fields": [
            {
                "field_id": beam.field.field_id,
                "phase_dir": {
                    "ra": beam.field.phase_dir.ra.deg.tolist(),
                    "dec": beam.field.phase_dir.dec.deg.tolist(),
                    "reference_time": beam.field.phase_dir.reference_time,
                    "reference_frame": beam.field.phase_dir.reference_frame,
                },
            }
            for beam in scan_type.beams
        ],
    }