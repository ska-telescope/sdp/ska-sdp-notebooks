{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Access to Pointing pipeline outputs**\n",
    "Last updated: 24-Feb-2025"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [pointing offset pipeline](https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/) calculates pointing offsets from \"pointing\"-type scan data. It fits the primary beam to the gain amplitudes of each dish and provides output files in HDF5 format containing the results.\n",
    "\n",
    "The pointing-offset processing script orchestrates the execution of the pipeline from within the Science Data Processor (SDP). More details about the pointing-offset script can be found in the [Pointing Documentation](https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/tree/master/src/ska-sdp-script-pointing-offset)\n",
    "\n",
    "The purpose of this notebook is to demonstrate how to access the output files from the pointing pipeline for a set of specified Execution Blocks. This would allow the pointing offsets to be used in further processing such as creating a global pointing model.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Import all the required packages - this notebook uses standard Python packages and can be run independently from the rest of the SDP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# IMPORTS\n",
    "\n",
    "import h5py\n",
    "import os\n",
    "import requests\n",
    "import yaml\n",
    "\n",
    "from glob import glob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "source": [
    "## Find pointing outputs corresponding to Execution Blocks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Specify the list of Execution Blocks that you are interested in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "execution_block_ids = [\"eb-orcatest-20240814-94773\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And specify the point at which the data PVC is mounted. If running this notebook from binderhub on the DP platform, the mount point will be `/shared`. Otherwise, it could be `/mnt/data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pvc_mount = \"/shared\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each Execution Block for a pointing observation contains several processing blocks. The relevant \"pointing-offset\" processing blocks can be identified by examining the ```ska-data-product.yaml``` file that is included in each processing block product directory. For \"pointing-offset\" processing blocks, this yaml file lists the output HDF5 files containing the pointing offset data products."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for execution_block_id in execution_block_ids:\n",
    "\n",
    "    print(f\"\\nPointing output files for {execution_block_id}:\\n\")\n",
    "\n",
    "    # Each EB has a directory on the shared storage\n",
    "    directory_to_eb = f\"{pvc_mount}/product/{execution_block_id}/ska-sdp\"\n",
    "\n",
    "    # Processing blocks within the EB are contained in separate directories\n",
    "    # The pointing processing blocks are defined by the data product yaml file.\n",
    "    product_yaml_files = glob(f\"{directory_to_eb}/**/ska-data-product.yaml\", recursive=True)\n",
    "\n",
    "    # Extract the pointing result file locations from 'pointing-offset' processing blocks\n",
    "    pointing_files = []\n",
    "    for yaml_file in product_yaml_files:\n",
    "        with open(yaml_file, \"r\", encoding=\"utf8\") as file:\n",
    "                data = yaml.safe_load(file)\n",
    "        if data[\"config\"][\"processing_script\"] == \"pointing-offset\":\n",
    "            for pointing_file in data[\"files\"]:\n",
    "                pointing_files.append(pointing_file)\n",
    "                print(f\"   {pointing_file['description']}:\")\n",
    "                print(f\"   {pointing_file['path']}\")\n",
    "\n",
    "    if not pointing_files:\n",
    "        print(\"   None found\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read pointing output files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pointing output files are in HDF5 format, and can be read using the Python ```h5py``` library.\n",
    "\n",
    "Note that the paths in the data product yaml file are specified without the mount point, so this must be added to obtain the full file path."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For example, take the last pointing file from the list above\n",
    "file_name = f\"{pvc_mount}{pointing_file['path']}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, open the file to find the pointing offsets and other parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Open the HDF5 file\n",
    "pointing = h5py.File(file_name,\"r\")\n",
    "\n",
    "# List the data keys\n",
    "pointing_keys = list(pointing.keys())\n",
    "print(pointing_keys)\n",
    "\n",
    "\n",
    "# List the attributes\n",
    "for attr in pointing[pointing_keys[0]].attrs.keys():\n",
    "    print(f\"{attr}:   {pointing[pointing_keys[0]].attrs[attr]}\")\n",
    "\n",
    "\n",
    "# Access the pointing table\n",
    "print(pointing[pointing_keys[0]].keys())\n",
    "\n",
    "# Pointing offsets\n",
    "print(pointing[pointing_keys[0]][\"data_pointing\"][()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The contents of the HDF5 file are in the format of a [PointingTable](https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_model.py?ref_type=heads#L389) object, and so alternatively, the file can be read back directly into this format by using [ska-sdp-datamodels](https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels) functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ska_sdp_datamodels.calibration import import_pointingtable_from_hdf5\n",
    "\n",
    "# Import the HDF5 file as a PointingTable object\n",
    "pointing_table = import_pointingtable_from_hdf5(file_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The PointingTable object contains all of the data and attributes from the file,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show the contents of the PointingTable\n",
    "pointing_table"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with the pointing offsets accessed as follows. Note that the key name here is `pointing` whereas in the HDF5 file, it is `data_pointing`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pointing offsets\n",
    "print(pointing_table[\"pointing\"].data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the Data Product Dashboard"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Data Product Dashboard gives an alternative interface to access data stored in the ``shared`` PVC on the DP platform. It consists of a [direct web interface](https://sdhp.stfc.skao.int/integration-ska-dataproduct-dashboard/dashboard/) and a [data product API](https://developer.skao.int/projects/ska-dataproduct-api/en/latest/userguide/overview.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code shows how to access the test execution block from this notebook and copy it to your local computer.\n",
    "\n",
    "The ``BASE_URL`` is also given below for use in a local script, in which case, the ``download_dir`` could be set to a local directory.\n",
    "\n",
    "At present, the connection only works from inside an SKAO cluster or when connected to the STFC VPN (top two options for `BASE_URL`). For instructions to connect to the STFC VPN, refer to this page: [Connecting to the STFC VPN](https://confluence.skatelescope.org/display/SWSI/Connecting+to+the+STFC+VPN)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up the dashboard BASE_URL to point to currently deployed dashboard API instance (the endpoint of the API)\n",
    "\n",
    "# If you are starting a script/Jupyter notebook inside an SKAO cluster (e.g. using Binderhub),\n",
    "BASE_URL = \"http://ska-dataproduct-dashboard-api-service.integration-ska-dataproduct-dashboard:8000\"\n",
    "\n",
    "# If you are connected to the STFC VPN and want to access the API locally through the command line or a local script,\n",
    "#BASE_URL = \"http://k8s.sdhp.skao/integration-ska-dataproduct-dashboard/api/\"\n",
    "\n",
    "# If you are outside the VPN, access currently does not work. But once it is fixed, the URL will be:\n",
    "#BASE_URL = \"https://sdhp.stfc.skao.int/integration-ska-dataproduct-dashboard/api\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Search for the example execution block\n",
    "execution_block_id = execution_block_ids[0]\n",
    "\n",
    "metadata = {\n",
    "    \"start_date\": \"2001-12-12\",\n",
    "    \"end_date\": \"2032-12-12\",\n",
    "    \"key_value_pairs\": [f\"execution_block:{execution_block_id}\"]\n",
    "}\n",
    "response = requests.post(f\"{BASE_URL}/dataproductsearch\", json=metadata)\n",
    "products = response.json()\n",
    "\n",
    "# Select the product from the 'pointing-offset' processing block\n",
    "for product in products:\n",
    "    if product['config.processing_script'] == \"pointing-offset\":\n",
    "        break\n",
    "print(product)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a new test directory in the notebook workspace\n",
    "download_dir = \"test_data/\"\n",
    "os.makedirs(download_dir, exist_ok=True)\n",
    "\n",
    "data = {\"fileName\": product[\"dataproduct_file\"],\"relativePathName\": product[\"dataproduct_file\"]}\n",
    "response = requests.post(f\"{BASE_URL}/download\", json=data)\n",
    " \n",
    "with open(f'{download_dir}/{execution_block_id}.tar', 'wb') as fd:\n",
    "    for chunk in response.iter_content(chunk_size=4096):\n",
    "        fd.write(chunk)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new folder, ``test_data`` will now be visible in the notebook workspace and should contain the execution block data in a tar file. This file can be downloaded from the notebook workspace onto your local computer: locate the newly created folder in the finder window to the left, and navigate to the tar file. Right click on the file to show the drop-down menu and select \"Download\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If needed it is possible to manually re-index the storage to ensure any new products are included\n",
    "response = requests.get(f\"{BASE_URL}/reindexdataproducts\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
