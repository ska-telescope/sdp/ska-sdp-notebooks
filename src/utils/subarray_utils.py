import json
import logging
import time

import ska_ser_logging
from tango import DevFailed, DevState

TIMEOUT = 60.0  # seconds
INTERVAL = 0.5  # seconds

logger = logging.getLogger(__name__)
ska_ser_logging.configure_logging(level=logging.INFO)

def wait_for_predicate(
    predicate, description, timeout=TIMEOUT, interval=INTERVAL
):
    """
    Wait for predicate to be true.

    :param predicate: callable to test
    :param description: description to use if test fails
    :param timeout: timeout in seconds
    :param interval: interval between tests of the predicate in seconds

    """
    start = time.time()
    while True:
        if predicate():
            break
        if time.time() >= start + timeout:
            raise TimeoutError(
                f"{description} not achieved after {timeout} seconds"
            )
        time.sleep(interval)


def wait_for_state(device, state, timeout=TIMEOUT):
    """
    Wait for device state to have the expected value.

    :param device: device client
    :param state: the expected state
    :param timeout: timeout in seconds
    """

    def predicate():
        return device.state() == state

    description = f"Device state {state.name}"
    logger.info(f"Waiting for device state {state.name}...")
    wait_for_predicate(predicate, description, timeout=timeout)


def wait_for_obs_state(device, obs_state, timeout=TIMEOUT):
    """
    Wait for obsState to have the expected value.

    :param device: device proxy
    :param obs_state: the expected value
    :param timeout: timeout in seconds
    """

    def predicate():
        return device.obsState == obs_state

    description = f"obsState {obs_state.name}"
    logger.info(f"Waiting for device obs_state {obs_state.name}...")
    wait_for_predicate(predicate, description, timeout=timeout)
    logger.info(f"Device obs_state {obs_state.name} has been reached")


def subarray_safe_release(device):
    """
    Safely releases subarray tango device to EMPTY obsState
    """
    if device.obsState == device.obsState.SCANNING:
        logger.info(">> End Scan")
        device.EndScan()
        wait_for_obs_state(device, device.obsState.READY)

    if device.obsState == device.obsState.READY:
        logger.info(">> End")
        device.End()
        wait_for_obs_state(device, device.obsState.IDLE)

    try:
        if device.obsState == device.obsState.IDLE:
            logger.info(">> Releasing All Resources")
            device.ReleaseAllResources()
            wait_for_obs_state(device, device.obsState.EMPTY)
    except DevFailed:
        # Execution block in progress may occur if never configured
        if device.obsState == device.obsState.IDLE:
            logger.info(">> Configure")
            scan_type_ids = list(
                filter(
                    lambda v: v != "interface",
                    json.loads(device.receiveAddresses).keys(),
                )
            )
            device.Configure(
                json.dumps(
                    {
                        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
                        "scan_type": scan_type_ids[0],
                    }
                )
            )
            wait_for_obs_state(device, device.obsState.READY)
            device.End()
            wait_for_obs_state(device, device.obsState.IDLE)

    if device.obsState == device.obsState.IDLE:
        logger.info(">> Releasing All Resources")
        device.ReleaseAllResources()
        wait_for_obs_state(device, device.obsState.EMPTY)

    if device.obsState == device.obsState.FAULT:
        device.Restart()
        wait_for_obs_state(device, device.obsState.EMPTY)

    assert device.obsState == device.obsState.EMPTY
    logger.info("Tango Device is EMPTY")


def subarray_safe_off(device):
    """
    Safely turns the subarray tango device to OFF state
    """
    subarray_safe_release(device)
    if device.state() == DevState.ON:
        logger.info(">> Device OFF")
        device.Off()
        wait_for_state(device, DevState.OFF)

    assert device.state() == DevState.OFF
    logger.info("Tango Device is OFF")


