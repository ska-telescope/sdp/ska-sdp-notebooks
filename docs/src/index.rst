SDP Notebooks
=============

The `SDP Notebooks GitLab repository <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks>`_
contains Jupyter Notebooks, which are intended to be used with
:doc:`BinderHub <developer.skao.int:tools/binderhub>`
set up by the System Team. These notebooks were developed
to be used as a guide and tutorial of how to run and interact with
the Science Data Processor (SDP).

.. toctree::
  :maxdepth: 2

  ska-binderhub
  sdp-notebooks
