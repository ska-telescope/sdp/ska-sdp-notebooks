import time
from dataclasses import dataclass
from typing import Iterable, Union

import numpy as np


@dataclass
class ProfilerRecord:
    nbytes: float
    send_time: Union[float, None]
    receive_time: float

    def __init__(
        self,
        nbytes: int,
        send_time: Union[float, None] = None,
        receive_time: Union[float, None] = None,
    ):
        self.nbytes = nbytes
        self.send_time = send_time
        self.receive_time = (
            receive_time if receive_time is not None else time.perf_counter()
        )


class Profiler:
    """
    Stream profiler for measuring and logging the
    throughput, latency and concurrency of a pipeline.
    """

    def __init__(self, nstreams=1):
        self.streams = []
        for _ in range(nstreams):
            self.streams.append([])

    def start(self):
        self.start_time = time.perf_counter()

    def record(self, nbytes: int, send_time: Union[float, None] = None, sidx: int = 0):
        self.streams[sidx].append(
            ProfilerRecord(nbytes, send_time, time.perf_counter())
        )

    def _throughput_report(self, sidx: int = 0) -> str:
        """Generates a report string for throughput.
        """
        # net throughput = total_nbytes / (last_receive_time - first_receive_time)
        output = []
        stream = self.streams[sidx]
        total_bytes = np.sum([record.nbytes for record in stream])
        throughput = total_bytes / (stream[-1].receive_time - stream[0].receive_time)
        output.append(f"{total_bytes/2**20:,} MiB : ")
        output.append(f"{throughput/2**20:,} MiB/s\n")
        return "".join(output)

    def _latency_report(self, sidx: int = 0, end: Union[int, None] = None) -> str:
        """Generates a report string for latency for the start of the stream to the
        end argument.
        """
        # total latency = last_receive_time - first_start_time
        output = []
        total_bytes = np.sum([record.nbytes for record in self.streams[sidx][:end]])
        latency = (
            self.streams[sidx][end - 1 if end is not None else -1].receive_time
            - self.start_time
        )
        output.append(f"{total_bytes/2**20:,} MiB : ")
        output.append(f"{latency/10**-3:,} ms\n")
        return "".join(output)

    def report(self, streams: Iterable[int] = range(1), num_chunk_messages: int = -1) -> str:
        """Generates a report string from the stream profile records.

        Args:
            streams (range, optional): stream id to report.
            num_chunk_messages (int, optional): Number of chunks per quantifiable payload.
        """
        output = []
        for sidx in streams:
            output.append(f"===Stream {sidx}===\n")
            output.append("1st Solution Latency:\n")
            output.append(self._latency_report(sidx, end=num_chunk_messages))
            output.append("Total Latency:\n")
            output.append(self._latency_report(sidx, end=None))
            output.append("Net Throughput:\n")
            output.append(self._throughput_report(sidx))
        return "".join(output)
