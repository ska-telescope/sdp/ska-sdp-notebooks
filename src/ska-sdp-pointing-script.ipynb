{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# **Running the Pointing offset processing script**\n",
    "Last updated: 27-Feb-2025"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [pointing offset script](https://developer.skao.int/projects/ska-sdp-script/en/latest/scripts/pointing-offset.html) deploys the [pointing offset calibration pipeline](https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/)\n",
    "which calculates the pointing offsets from \"pointing\"-type scan data.\n",
    "\n",
    "To deploy SDP, follow the instruction at [Installing SDP](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/standalone.html). You will need to deploy\n",
    "on the Data Processing Platform (see [further instructions here](https://developer.skao.int/projects/ska-sdp-integration/en/latest/installation/remote-cluster.html#data-processing-dp-cluster)\n",
    "SDP will need to be deployed with access to the `shared` PVC. You can achieve this by setting the following helm value `global.data-product-pvc-name: shared`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up tango and data for processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Import all the required packages and define `namespace`, `databaseds` service and set the `TANGO_HOST`.\n",
    "\n",
    "Each Tango Controls system/deployment has to have at least one running `databaseds` device server. The machine on which\n",
    "`databaseds` device server is running has a role called `TANGO_HOST` and this needs to be set to a remote host."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# IMPORTS\n",
    "\n",
    "import os\n",
    "import json\n",
    "import random\n",
    "import logging\n",
    "import ska_ser_logging\n",
    "import h5py\n",
    "\n",
    "from asyncio import sleep\n",
    "from datetime import date\n",
    "from tango import DeviceProxy, DevFailed, Database\n",
    "\n",
    "from utils.pointing_script_utils import cbf_scan\n",
    "from utils.subarray_utils import wait_for_obs_state, wait_for_state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# configure ska logging, change as needed \n",
    "logger = logging.getLogger(__name__)\n",
    "ska_ser_logging.configure_logging(level=logging.INFO)\n",
    "\n",
    "# specify here the control system namespace to connect in this cluster\n",
    "KUBE_NAMESPACE = \"<update-with-ns!!!>\"\n",
    "\n",
    "# set the name of the databaseds service\n",
    "DATABASEDS_NAME = \"databaseds-tango-base\"\n",
    "\n",
    "# set the KAFKA_HOST\n",
    "KAFKA_HOST = f\"ska-sdp-kafka.{KUBE_NAMESPACE}:9092\"\n",
    "\n",
    "# set the TANGO_HOST\n",
    "os.environ[\"TANGO_HOST\"] = f\"{DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.cluster.local:10000\"\n",
    "\n",
    "# set connection to the Configuration Database\n",
    "os.environ[\"SDP_CONFIG_HOST\"] = f\"ska-sdp-etcd-client.{KUBE_NAMESPACE}\"\n",
    "\n",
    "# Receptors used for processing\n",
    "RECEPTORS = [ \"SKA001\", \"SKA036\", \"SKA063\", \"SKA100\" ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The below cell creates three Kafka topics with custom properties. These are topics that the mswriter pod uses to read pointing data from. We update the retention on these pods so that the topics are cleared in a timely manner, which means, rerunning the notebook would not result in failure of the pointing pipeline due to duplicated entries read from these Kafka topics.\n",
    "\n",
    "See also (and related pages): https://confluence.skatelescope.org/display/SE/PI24.4+Kafka+retaining+old+entries+-+investigation\n",
    "\n",
    "Note that SDP needs to be installed with the following kafka settings for the deletion of data to take effect quickly:\n",
    "\n",
    "    kafka:\n",
    "        logRetentionCheckIntervalMs: \"10000\"\n",
    "        \n",
    "This will reset the retention check interval to 10 seconds.\n",
    "\n",
    "[!IMPORTANT]\n",
    "Only run the following cell once per life of the Kafka pod!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# ONLY RUN ONCE! \n",
    "# Only rerun this cell if you have restarted the kafka pod first.\n",
    " \n",
    "from kafka.admin import NewTopic, KafkaAdminClient\n",
    " \n",
    "# these are the topics that the mswriter reads the pointing data from;\n",
    "# we need to remove the data from these to prevent duplicate entries in the MS\n",
    "topics = [\"commanded-pointings\", \"actual-pointings\", \"source-offsets\"]\n",
    " \n",
    "# segment.ms: sets the segment retention time\n",
    "# retention.ms: sets the individual message retention time\n",
    "# retention.bytes: I read somewhere that this needs to be reset to -1 so that the retention time can take effect (it's the retention of logs in size)\n",
    "topic_config = {\"segment.ms\": 5000, \"retention.ms\": 5000, \"retention.bytes\": -1}\n",
    " \n",
    "new_topics = []\n",
    " \n",
    "admin = KafkaAdminClient(bootstrap_servers=KAFKA_HOST)\n",
    " \n",
    "for topic in topics:\n",
    "    new_topics.append(NewTopic(name=topic, num_partitions=1, replication_factor=1, topic_configs=topic_config))\n",
    " \n",
    "admin.create_topics(new_topics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The following step is only needed, if a development version of the pointing-offset script is used!\n",
    "\n",
    "Note, that this will fail, if the version already exists in the Config DB. This failure will not break SDP or any of the following steps. Uncomment the content of the cell and update with the relevant image and script version (replace the placeholder `<>`s) as needed. If the version already exists, and you want to replace it, you can use `ska-sdp update script ...` to do that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "# Add the pointing processing script definition\n",
    "!ska-sdp create script realtime:pointing-offset:<> '{\"image\": \"<>\"}'\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to download the MeasurementSet (MS) data files that the pointing pipeline will process to the location where the CBF emulator can access them for sending over to vis-receive. In addition, we also need to provide the pointing data (commanded, actual pointings, and source offsets), which are stored in HDF files. These will be saved in the MS files during the receive process, and are essential for the pointing offset calibration pipeline to run.\n",
    "\n",
    "Both the MS data and the pointing data can be found in the [SDP Integration repository](https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/tree/master/tests/resources/data/pointing-data), in a tar file. The data are simulated 5 scans with the SKA AA0.5 configuration at Band 2. They have been\n",
    "reordered in ANTENNA and TIME columns to match the baseline order expected by the receiver (see README of the tar file).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "DATA_DIR = \"/shared/pointing_offset_tests/pointing_data\"\n",
    "HDF_DATA_DIR = f\"{DATA_DIR}/pointing-hdfs\"\n",
    "\n",
    "DATA_URL = \"https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/raw/master/tests/resources/data/pointing-data/pointing-data.tar\"\n",
    "\n",
    "if not os.path.isdir(DATA_DIR):\n",
    "    os.makedirs(DATA_DIR)\n",
    "    !cd $DATA_DIR\n",
    "    !curl -O $DATA_URL\n",
    "    !tar -xvf pointing-data.tar \n",
    "\n",
    "else:\n",
    "     print(f\"Data are already available in {DATA_DIR}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "MS_SCANS = [\n",
    "    \"scan-1.ms\", \n",
    "    \"scan-2.ms\", \n",
    "    \"scan-3.ms\", \n",
    "    \"scan-4.ms\", \n",
    "    \"scan-5.ms\"\n",
    "]\n",
    "\n",
    "ACTUAL_HDF_SCANS = [\n",
    "    \"actual_pointing_scan-1.hdf\", \n",
    "    \"actual_pointing_scan-2.hdf\", \n",
    "    \"actual_pointing_scan-3.hdf\", \n",
    "    \"actual_pointing_scan-4.hdf\", \n",
    "    \"actual_pointing_scan-5.hdf\"\n",
    "]\n",
    "\n",
    "REQUESTED_HDF_SCANS = [\n",
    "    \"requested_pointing_scan-1.hdf\", \n",
    "    \"requested_pointing_scan-2.hdf\", \n",
    "    \"requested_pointing_scan-3.hdf\", \n",
    "    \"requested_pointing_scan-4.hdf\", \n",
    "    \"requested_pointing_scan-5.hdf\"\n",
    "]\n",
    "\n",
    "SOURCE_OFFSET_HDF_SCANS = [\n",
    "    \"source_offset_scan-1.hdf\", \n",
    "    \"source_offset_scan-2.hdf\", \n",
    "    \"source_offset_scan-3.hdf\", \n",
    "    \"source_offset_scan-4.hdf\", \n",
    "    \"source_offset_scan-5.hdf\"\n",
    "]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Deploying the Mock dish devices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "In a real observation, pointing data is obtained directly from the dish tango devices. The SDP Queue Connector device is configured to expect the data on tango attributes (which attributes that is defined in vis-receive parameters). Therefore, we need to substitute the real dish LMC, with an alternative, which, in SDP Integration is done using the [mock dish devices](https://gitlab.com/ska-telescope/sdp/ska-sdp-mock-dish-devices).\n",
    "\n",
    "You need to deploy them with access to the shared PVC, so that they can load and use the pointing HDF files you downloaded in the previous step.\n",
    "\n",
    "Steps to get the mock dishes running:\n",
    "1. Use the chart that is available in ska-sdp-integration repository: [mock-dish](https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/tree/master/tests/resources/charts/mock-dish)\n",
    "2. Create a custom values.yaml file with the following content; let's call it test-values.yaml:\n",
    "   <div class=\"alert alert-block alert-info\">\n",
    "   \n",
    "   ```\n",
    "   mock_dish:\n",
    "     receptors: {RECEPTORS}\n",
    "     dishMaster: \n",
    "       achievedPointing:\n",
    "         1: {HDF_DATA_DIR}/actual_pointing_scan-1.hdf\n",
    "         2: {HDF_DATA_DIR}/actual_pointing_scan-2.hdf\n",
    "         3: {HDF_DATA_DIR}/actual_pointing_scan-3.hdf\n",
    "         4: {HDF_DATA_DIR}/actual_pointing_scan-4.hdf\n",
    "         5: {HDF_DATA_DIR}/actual_pointing_scan-5.hdf\n",
    "     dishLeafnode:\n",
    "       desiredPointing:\n",
    "         1: {HDF_DATA_DIR}/requested_pointing_scan-1.hdf\n",
    "         2: {HDF_DATA_DIR}/requested_pointing_scan-2.hdf\n",
    "         3: {HDF_DATA_DIR}/requested_pointing_scan-3.hdf\n",
    "         4: {HDF_DATA_DIR}/requested_pointing_scan-4.hdf\n",
    "         5: {HDF_DATA_DIR}/requested_pointing_scan-5.hdf\n",
    "       sourceOffset:\n",
    "         1: {HDF_DATA_DIR}/source_offset_scan-1.hdf\n",
    "         2: {HDF_DATA_DIR}/source_offset_scan-2.hdf\n",
    "         3: {HDF_DATA_DIR}/source_offset_scan-3.hdf\n",
    "         4: {HDF_DATA_DIR}/source_offset_scan-4.hdf\n",
    "         5: {HDF_DATA_DIR}/source_offset_scan-5.hdf\n",
    "   \n",
    "   pvc:\n",
    "     name: shared\n",
    "     path: /shared\n",
    "   ```\n",
    "   \n",
    "   </div>\n",
    "   Replace the following:\n",
    "     - `{RECEPTORS}`: RECEPTORS defined in the second cell\n",
    "     - `{HDF_DATA_DIR}`: HDF_DATA_DIR defined two cells up\n",
    "   Note that the numbers associated with the pointing files are the Scan IDs.\n",
    "3. Install the chart:\n",
    "   ```\n",
    "   helm install mock-dish ska-sdp-integration/tests/resources/charts/mock-dish -f test-values.yaml -n <control-system-namespace>\n",
    "   ```\n",
    "   Replace `<control-system-namespace>` with KUBE_NAMESPACE defined in the second cell. Update the path to the chart as needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## AssignResources, Configure, and Scan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's start by obtaining a handle of the subarray tango device, then check if the device is ON and the obstate is `EMPTY`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "subarray_device = DeviceProxy(\"test-sdp/subarray/01\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    subarray_device.On()\n",
    "except DevFailed:\n",
    "    print(\"The device is already in ON state!\")\n",
    "\n",
    "subarray_device.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The next step is to set up the configuration string. The following example will deploy one processing block for pointing-offset and another for vis-receive. In the following JSON string, update the image and version for pointing-offset as needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "generator = \"notebook\"\n",
    "today = date.today().strftime(\"%Y%m%d\")\n",
    "number = random.randint(0, 99998)\n",
    "\n",
    "EXECUTION_BLOCK_ID = f\"eb-{generator}-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_REALTIME = f\"pb-{generator}vr-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_POINTING = f\"pb-{generator}po-{today}-{number:05d}\"\n",
    "\n",
    "TOTAL_CHANNELS = 8  # should match emulator data\n",
    "\n",
    "config = {\n",
    "    \"interface\": \"https://schema.skao.int/ska-sdp-assignres/1.0\",\n",
    "    \"resources\": {\n",
    "        \"receptors\": RECEPTORS,\n",
    "        \"receive_nodes\": 1,\n",
    "    },\n",
    "    \"execution_block\": {\n",
    "        \"eb_id\": EXECUTION_BLOCK_ID,\n",
    "        \"context\": {},\n",
    "        \"max_length\": 21600.0,\n",
    "        \"channels\": [\n",
    "            {\n",
    "                \"channels_id\": \"vis_channels\",\n",
    "                 \"spectral_windows\": [{\n",
    "                     \"spectral_window_id\": \"fsp_1_channels\",\n",
    "                     \"count\": TOTAL_CHANNELS,\n",
    "                     \"start\": 0,\n",
    "                     \"stride\": 1,\n",
    "                     \"freq_min\": 1.2925e9,\n",
    "                     \"freq_max\": 1.4125e9,\n",
    "                     \"link_map\": [ [0, 0], [200, 1], [744, 2], [944, 3] ]\n",
    "                    }]\n",
    "            }\n",
    "        ],\n",
    "        \"polarisations\": [\n",
    "            {\n",
    "                \"polarisations_id\": \"all\",\n",
    "                \"corr_type\": [\"XX\", \"XY\", \"YX\", \"YY\"],\n",
    "            }\n",
    "        ],\n",
    "        \"fields\": [\n",
    "            {\n",
    "                \"field_id\": \"field_a\",\n",
    "                \"phase_dir\": {\n",
    "                    \"target_name\": \"target_a\",\n",
    "                    \"reference_frame\": \"icrs\",\n",
    "                    \"attrs\": {\n",
    "                        \"c1\": 2.711325,\n",
    "                        \"c2\": -0.01328889,\n",
    "                        \"epoch\": 2000.0\n",
    "                    }\n",
    "                },\n",
    "                \"pointing_fqdn\": \"low-tmc/telstate/0/pointing\",\n",
    "            },\n",
    "            {\n",
    "                \"field_id\": \"field_b\",\n",
    "                \"phase_dir\": {\n",
    "                    \"target_name\": \"target_b\",\n",
    "                    \"reference_frame\": \"icrs\",\n",
    "                    \"attrs\": {\n",
    "                        \"c1\": 294.8629,\n",
    "                        \"c2\": -63.44029,\n",
    "                        \"epoch\": 2000.0\n",
    "                    }\n",
    "                },\n",
    "                \"pointing_fqdn\": \"low-tmc/telstate/0/pointing\",\n",
    "            },\n",
    "        ],\n",
    "        \"beams\": [{\"beam_id\": \"vis0\", \"function\": \"visibilities\"}],\n",
    "        \"scan_types\": [\n",
    "            {\n",
    "                \"scan_type_id\": \".default\",\n",
    "                \"beams\": {\n",
    "                    \"vis0\": {\n",
    "                        \"polarisations_id\": \"all\",\n",
    "                        \"channels_id\": \"vis_channels\",\n",
    "                    }\n",
    "                },\n",
    "            },\n",
    "            {\n",
    "                \"scan_type_id\": \"science\",\n",
    "                \"derive_from\": \".default\",\n",
    "                \"beams\": {\"vis0\": {\"field_id\": \"field_a\"}},\n",
    "            },\n",
    "            {\n",
    "                \"scan_type_id\": \"pointing\",\n",
    "                \"derive_from\": \".default\",\n",
    "                \"beams\": {\"vis0\": {\"field_id\": \"field_b\"}},\n",
    "            },\n",
    "        ],\n",
    "    },\n",
    "    \"processing_blocks\": [\n",
    "        {\n",
    "            \"pb_id\": PROCESSING_BLOCK_ID_POINTING,\n",
    "             \"script\":{\n",
    "                \"kind\":\"realtime\",\n",
    "                \"name\":\"pointing-offset\",\n",
    "                \"version\":\"1.0.0\"\n",
    "             },\n",
    "             \"parameters\": {\n",
    "                \"num_scans\": 5,\n",
    "                \"additional_args\": [\"--use_source_offset_column\", \n",
    "                                    \"--use_modelvis\",\n",
    "                                    \"--num_chunks\",  \"8\",\n",
    "                                    \"--bw_factor\", \"0.95\", \"0.95\"]\n",
    "             },\n",
    "             \"dependencies\": [\n",
    "                 {\n",
    "                     \"pb_id\": PROCESSING_BLOCK_ID_REALTIME, \"kind\": [\"vis-receive\"]\n",
    "                 }\n",
    "             ],\n",
    "        },\n",
    "        {\n",
    "            \"pb_id\": PROCESSING_BLOCK_ID_REALTIME,\n",
    "            \"script\": {\n",
    "                \"kind\": \"realtime\",\n",
    "                \"name\": \"vis-receive\",\n",
    "                \"version\": \"5.0.0\",\n",
    "            },\n",
    "            \"parameters\": {\n",
    "                \"channels_per_port\": TOTAL_CHANNELS,\n",
    "                \"telstate\": {\n",
    "                    \"target_fqdn\": \"tango://test-sdp/mockdishleafnode/{dish_id}/desiredPointing\",\n",
    "                    \"source_offset_fqdn\": \"tango://test-sdp/mockdishleafnode/{dish_id}/sourceOffset\",\n",
    "                    \"direction_fqdn\": \"tango://test-sdp/mockdishmaster/{dish_id}/achievedPointing\"\n",
    "                },\n",
    "                \"transport_protocol\": \"tcp\",\n",
    "                \"extra_helm_values\": {\n",
    "                    \"receiver\": {\n",
    "                        \"options\": {\n",
    "                           \"reception\": {\n",
    "                               \"reset_time_indexing_after_each_scan\": True\n",
    "                           },\n",
    "                           \"telescope_model\": {\n",
    "                               \"telmodel_key\": \"instrument/ska1_mid/layout/mid-layout.json\"\n",
    "                           }\n",
    "                        }\n",
    "                    },\n",
    "                },\n",
    "                \"pod_settings\": [\n",
    "                    {\"securityContext\": {\"runAsUser\": 0, \"fsGroup\": 0}}\n",
    "                ],\n",
    "                \"processors\": {\n",
    "                    \"mswriter\": {\n",
    "                        \"args\": [\n",
    "                            \"realtime.receive.processors.sdp.\"\n",
    "                            \"mswriter_processor.MSWriterProcessor\",\n",
    "                            \"--plasma_socket\",\n",
    "                            \"/plasma/socket\",\n",
    "                            \"--readiness-file=/tmp/processor_ready\",\n",
    "                            \"--commanded-pointing-topic=commanded-pointings\",\n",
    "                            \"--actual-pointing-topic=actual-pointings\",\n",
    "                            \"--source-offset-topic=source-offsets\",\n",
    "                            \"output.ms\",\n",
    "                        ],\n",
    "                    }\n",
    "                }\n",
    "            },\n",
    "            \"dependencies\": [\n",
    "                 {\n",
    "                     \"pb_id\": PROCESSING_BLOCK_ID_POINTING, \"kind\": [\"pointing-offset\"]\n",
    "                 }\n",
    "            ],\n",
    "        }\n",
    "    ],\n",
    "}\n",
    "\n",
    "config_eb = json.dumps(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's execute the AssignResources command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "subarray_device.assignResources(config_eb)\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.IDLE, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The AssignResources command will start a processing block for pointing-offset and one for vis-receive. These will trigger the deployment of\n",
    "two execution engines (one for the pointing offset calibration pipeline and another for the visibility receiver with MS writer).\n",
    "\n",
    "The pointing-offset script sets the `pointing_cal` key of receive addresses with the pointing FQDN, which can be viewed on the `receiveAddresses` tango attribute of the subarray device:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "json.loads(subarray_device.receiveAddresses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now, execute the Configure command by setting the scan_type to \"pointing\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "SCAN_TYPE_ID = \"pointing\"\n",
    "\n",
    "subarray_device.Configure(json.dumps(\n",
    "{\n",
    "    \"interface\": \"https://schema.skao.int/ska-sdp-configure/1.0\",\n",
    "    \"scan_type\": SCAN_TYPE_ID,\n",
    "}\n",
    "))\n",
    "wait_for_obs_state(subarray_device, subarray_device.obsState.READY, timeout=120)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "This will configure a pointing scan, which will trigger the pipeline to move to the next step, which is to wait for the right number of pointing scans to be executed. You can follow the progress of the pipeline via its logs.\n",
    "For this you will need KUBECONFIG access on your local machine. Then you can use `kubectl logs` or `k9s` to check the logs of the pod, which is running in the processing namespace (e.g. f\"{KUBE_NAMESPACE}-p\"). Alternatively, you can use [Kibana](https://k8s.stfc.skao.int/kibana/app/discover)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Next, execute Scans. The following code will run 5 Scans for the 5-scan pointing observation that is expected by the pipeline as defined by the `num_scans` parameter of the `pointing-offset` script in the AssignResources configuration string. Each time, it will turn the mock dish devices on for pointing data, which the QueueConnector will forward to the `mswriter`. It also calls the CBF emulator to send the MS data. The `mswriter` (part of `receive`) takes the data sent by the emulator and Kafka and writes them into a MeasurementSet. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "MS_DATA = {scan_id+1: f\"{DATA_DIR}/{MS_SCANS[scan_id]}\" for scan_id in range(5)}\n",
    "\n",
    "# Get Receive Addresses\n",
    "receiveAddresses = json.loads(subarray_device.receiveAddresses)\n",
    "\n",
    "# only send to 1st beam address\n",
    "beam_id = list(receiveAddresses[SCAN_TYPE_ID].keys())[0]\n",
    "host = receiveAddresses[SCAN_TYPE_ID][beam_id][\"host\"][0][1]\n",
    "start_port = receiveAddresses[SCAN_TYPE_ID][beam_id][\"port\"][0][1]\n",
    "\n",
    "# get mock dish devices\n",
    "db = Database()\n",
    "mock_dishes = [dish for dish in db.get_device_exported(\"*mockdish*\") if not dish.startswith(\"dserver\")]\n",
    "\n",
    "for scan_num in range(5):\n",
    "    scan_id = scan_num + 1\n",
    "      \n",
    "    print(\">> Scan %i\" % scan_id)\n",
    "    \n",
    "    dishes = []\n",
    "    for device in mock_dishes:\n",
    "        dish = DeviceProxy(device)\n",
    "        dish.Scan(scan_id)\n",
    "        dishes.append(dish)\n",
    "    for dish in dishes:\n",
    "        wait_for_state(dish, dish.state().ON)\n",
    "    \n",
    "    subarray_device.Scan(\n",
    "        json.dumps(\n",
    "            {\n",
    "                \"interface\": \"https://schema.skao.int/ska-sdp-scan/1.0\",\n",
    "                \"scan_id\": scan_id,\n",
    "            }\n",
    "        )\n",
    "    )\n",
    "    wait_for_obs_state(subarray_device, subarray_device.obsState.SCANNING)\n",
    "    \n",
    "    print(\"Emulating scan...\")\n",
    "    await cbf_scan(MS_DATA[scan_id], host, start_port, scan_id)\n",
    "    # to give time for receiver to be ready again \n",
    "    # and for the pointing data to be loaded from kafka:\n",
    "    #   sometimes the data are loaded slower from kafka then from the cbf emulator;\n",
    "    #   in the logs of vis-receive -> mswriter-processor you will see some \n",
    "    #   that say there were not enough pointing data for interpolation; \n",
    "    #   in this case, increase the sleep below, release resources and try again\n",
    "    await sleep(7)\n",
    "\n",
    "    print(\">> End Scan\")\n",
    "    subarray_device.EndScan()\n",
    "    \n",
    "    for dish in dishes:\n",
    "        dish.EndScan()\n",
    "    for dish in dishes:\n",
    "        wait_for_state(dish, dish.state().OFF)\n",
    "    \n",
    "    wait_for_obs_state(subarray_device, subarray_device.obsState.READY)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the pipeline should process the 5 MS that we saved in the $MS_DATA_DIR directory. You can monitor its progress via logs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing the results from the QueueConnector device"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The QueueConnector device monitors the Kafka topic where the pointing data are sent by the pipeline, and saves them to tango attributes. Each dish has its own tango attribute with format `<attribute_prefix>_<dish_name>` - here, the attribute prefix is set to `pointing_offset`.\n",
    "\n",
    "The attribute for each dish contains three floating point values: `[scan_id, xel_offset, el_offset]`, where `scan_id` is the ID of the last scan of the given pointing observation, and the offsets are given in degrees for cross-elevation (`xel`) and elevation (`el`).\n",
    "\n",
    "**NOTE**: \n",
    "\n",
    "    - We have to wait for the Scan execution to complete, and the pipeline to finish running, before turning on the QueueConnector. The progress of the Scan can be monitored by looking at the logs of the pods. After the pipeline finished, give the device a few extra seconds to load the data from Kafka (you may have to run the following cell a few times before the data are displayed).\n",
    "    \n",
    "    - Sometimes you will see data appearing for some dishes but others might show `nan`s. This can be fore the same reason as why we needed to introduce a sleep after running the CBF emulator code: see comment in the cell above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Connect to the QueueConnector device\n",
    "q = DeviceProxy(\"test-sdp/queueconnector/01\")\n",
    "\n",
    "# Each dish has its own tango attribute, here called pointing_offset_<dish_name>, for example \"pointing_offset_SKA001\". \n",
    "for dish_name in RECEPTORS:\n",
    "    print(\"%s: %s\" % (dish_name, q.read_attribute(f\"pointing_offset_{dish_name}\").value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now let's inspect the data products. Here, we point you to the directory where they are stored. Executing the cell below, you will see one or more subdirectories, which contain the data products for the given batch of pointing scans. These include an HDF5 file and a metadata yaml file, as well as figures and a txt file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "PVC_MOUNT= \"/shared\"\n",
    "!ls $PVC_MOUNT/product/$EXECUTION_BLOCK_ID/ska-sdp/$PROCESSING_BLOCK_ID_POINTING"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following notebook provides tools to find and inspect the output HDF5 files from any execution block: [ska-sdp-find-pointing-outputs.ipynb](https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-find-pointing-outputs.ipynb). \n",
    "\n",
    "In order to read the pointing offsets from the specific HDF file that we just created, use the following commands. Note that only the offsets (`data_pointing`) are printed here. The other fitted parameters can also be accessed in the HDF5 file by specifying their field names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the HDF5 file\n",
    "output_hdf_directory = f\"{PVC_MOUNT}/product/{EXECUTION_BLOCK_ID}/ska-sdp/{PROCESSING_BLOCK_ID_POINTING}/scan1-5\"\n",
    "output_hdf_filename = \"pointing_offsets_scans_1-5.hdf5\"\n",
    "with h5py.File(f\"{output_hdf_directory}/{output_hdf_filename}\",'r') as pointing:\n",
    "    # Pointing offsets\n",
    "    print(pointing[\"PointingTable0\"][\"data_pointing\"][()])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Debugging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "If the results on the QueueConnector device don't look right, or the output data is missing files/not as expected, a way to check whether any errors were produced by the pipeline is by looking at its processing block state. The following cell will display the entry from the Configuration Database. It contains a key called \"processed\", which is updated by the pointing pipeline after each run. This also lists any errors that may have occurred.\n",
    "\n",
    "You may also use this entry to check if the pipeline finished processing a batch of scans."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!ska-sdp get /pb/$PROCESSING_BLOCK_ID_POINTING/state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clean up"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, end the processing and return the subarray to an EMPTY obsState"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "subarray_device.end()  # end the process (even if obsState is IDLE)\n",
    "subarray_device.releaseAllResourceS()  # release resources and get obsState back to EMPTY"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "All the processing blocks should finish at this point"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, clean up the data directory for this execution block"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!rm -r $PVC_MOUNT/product/$EXECUTION_BLOCK_ID"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
