"""
Utils for running the pointing-script notebook
"""
import logging

from astropy.time import Time
from ska_sdp_cbf_emulator.data_source import MeasurementSetDataSourceConfig
from ska_sdp_cbf_emulator.packetiser import SenderConfig, packetise
from ska_sdp_cbf_emulator.transmitters.spead2_transmitters import (
    Spead2TransmissionConfig,
    TransportProtocol,
)

logger = logging.getLogger(__name__)

_MJD_TO_Y2000TAI_SEC: float = (
    Time("2000-01-01", format="isot", scale="tai") - Time(0.0, format="mjd")
).sec


async def cbf_scan(ms_path: str, target_host: str, target_port: str, scan_id: int):
    """
    Run the CBF emulator

    :param ms_path: path to MeasurementSet to be sent
    :param target_host: host address of vis-receive pod
    :param target_port: port of vis-receive pod
    :param scan_id: ID of the scan that is running
    :param total_channels: total number of channels in data
    """
    total_streams = 1  # total number of spead streams to use
    # number of times the emulator will repeat the scan data on disk
    num_repeats = 1

    sender_config = SenderConfig(scan_id=scan_id)

    sender_config.ms = MeasurementSetDataSourceConfig(
        location=ms_path, num_repeats=num_repeats
    )

    sender_config.transmission = Spead2TransmissionConfig(
        target_host=target_host,
        target_port_start=target_port,
        transport_protocol=TransportProtocol.TCP,
        rate=0,
        num_streams=total_streams,
    )

    await packetise(sender_config)
