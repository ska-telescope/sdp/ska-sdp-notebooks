# Changelog

## master

* Update the access to pointing outputs notebook ([MR74](https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/merge_requests/74))
* Update ska-sdp cli and tango-tutorials notebook for SDP v1 ([MR71](https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/merge_requests/71))
* Update the test-mock-data notebook for SDP v1 ([MR70](https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/merge_requests/70))

* Added the ska-sdp CLI notebook ([MR]())
* Initial files to set up repository, including docs,  gitlab-ci, etc ([MR]())
