import asyncio
import queue
import threading
from contextlib import AbstractContextManager
from typing import Callable, Union

from overrides import override


class SubscribeEventContext(AbstractContextManager):
    """
    Context manager for controlling the lifetime of a tango
    event subscription.
    """

    def __init__(self, proxy, attr_name, event_type, callback):
        self.__proxy = proxy
        self.__attr_name = attr_name
        self.__event_type = event_type
        self.__callback = callback

    @override
    def __enter__(self):
        self.__id = self.__proxy.subscribe_event(
            self.__attr_name, self.__event_type, self.__callback, stateless=True
        )
        return self

    @override
    def __exit__(self, exc_type, exc_value, traceback):
        self.__proxy.unsubscribe_event(self.__id)


class SubscribeEventConditionContext(SubscribeEventContext):
    """
    Context manager for a threadsafe asyncio listener of tango events.
    """

    def __init__(self, proxy, attr_name, event_type, loop=None):
        self.__condition = asyncio.Condition()
        self.__queue = queue.Queue()
        self.__queue.put_nowait(proxy.read_attribute(attr_name).value)
        loop = asyncio.get_running_loop() if loop is None else loop

        async def notify_condition(value):
            self.__queue.put_nowait(value)
            async with self.__condition:
                self.__condition.notify_all()

        def callback_threadsafe(event):
            value = event.attr_value.value
            asyncio.run_coroutine_threadsafe(notify_condition(value), loop)

        super().__init__(proxy, attr_name, event_type, callback_threadsafe)

    async def wait_for(self, value, timeout: Union[float, None]):
        """
        Waits for the context attribute to become a specified value.
        NOTE: simultaneous calls not supported.
        """

        def predicate():
            result = False
            while not result and not self.__queue.empty():
                result = value == self.__queue.get_nowait()
            return result

        async with self.__condition:
            try:
                await asyncio.wait_for(
                    self.__condition.wait_for(predicate),
                    timeout=timeout,
                )
            except asyncio.TimeoutError:
                current_value = self.__proxy.read_attribute(
                    self.__attr_name
                ).value
                raise asyncio.TimeoutError(
                    f"timout waiting for {current_value} to become {value}"
                )


class SubscribeEventTensorContext(SubscribeEventContext):
    """
    Context manager for controlling the lifetime of a tango
    event subscription with a callback that only processes
    event values.
    """

    def __init__(
        self,
        proxy,
        attr_name: str,
        attr_shape_name: str,
        event_type,
        callback: Callable,
    ):
        def conditional_callback(event):
            if (
                event
                and event.attr_value
                and threading.current_thread() is not threading.main_thread()
            ):
                callback(
                    event.attr_value.value.reshape(
                        proxy.read_attribute(attr_shape_name).value
                    )
                )

        super().__init__(proxy, attr_name, event_type, conditional_callback)
