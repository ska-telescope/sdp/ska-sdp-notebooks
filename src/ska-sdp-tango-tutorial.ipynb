{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# **Control SDP using the Tango devices**\n",
    "\n",
    "Last Updated: 24-Feb-2025"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Controlling the Science Data Processor (SDP) using Tango devices allows to directly interact with the devices such as querying,\n",
    "changing attributes and executing commands directly to Tango.\n",
    "\n",
    "The Tango devices in the SDP are:\n",
    "   - The SDP controller Tango device is designed to provide the overall control of the SDP. The commands it receives cause\n",
    "     the other SDP services to be stopped or started, and its attributes report on the overall state of the system.\n",
    "   - The SDP subarray Tango device is the principal means by which processing is initiated in the SDP.\n",
    "   - The SDP queue connector device moves data between various sources and sinks via data queues (Kafka). In this notebook, we do not interact with it. To work with it, check out our other notebooks in this repository.\n",
    "\n",
    "Please note this notebook currently works with SDP v1.0.0.\n",
    "\n",
    "If you get stuck you can look at the official documentation pages:\n",
    " - [SDP Integration](https://developer.skao.int/projects/ska-sdp-integration/en/latest/index.html)\n",
    " - [SDP Local Monitoring and Control](https://developer.skao.int/projects/ska-sdp-lmc/en/latest/index.html)\n",
    " - [SDP Processing Scripts](https://developer.skao.int/projects/ska-sdp-script/en/latest/)\n",
    " - [SDP on the Developer Portal](https://developer.skao.int/en/latest/projects/area/sdp.html)\n",
    "\n",
    "If you still don't find the answer to your questions, contact us on Slack: #help-sdp\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## 1. Import packages and set environments\n",
    "\n",
    "The SDP contains Tango device servers which control different aspects of the system. Each Tango Controls system/deployment has to have at least one running `databaseds` device server. The machine on which the `databaseds` device server is running has a role called Tango Host. `Databaseds` is a device server providing configuration information to all other components of the system as well as a runtime catalog of the components/devices. It allows client applications to find devices in distributed environment. `TANGO_HOST` environment variable would need to be set to point to a remote host that is running the Configuration database.\n",
    "\n",
    "Import all the required packages and define `namespace`, `databaseds` service and set the `TANGO_HOST`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import os\n",
    "from tango import DeviceProxy, EventType\n",
    "from utils.subarray_utils import wait_for_obs_state\n",
    "\n",
    "# specify here the namespace to connect in this cluster\n",
    "KUBE_NAMESPACE = \"<update-with-ns!!!>\"\n",
    "\n",
    "# set the name of the databaseds service\n",
    "DATABASEDS_NAME = \"databaseds-tango-base\"\n",
    "\n",
    "# finally set the TANGO_HOST\n",
    "os.environ[\"TANGO_HOST\"] = f\"{DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.cluster.local:10000\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## 2. Accessing the Tango Interface\n",
    "\n",
    "Let's start by obtaining a handle of the subarray tango device:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d = DeviceProxy('test-sdp/subarray/01')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's check the state of the device"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.state()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The device should be in `OFF` state. This means the device is in inactive state\n",
    "\n",
    "Let's check subarray observing state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The above output would be `EMPTY`. This means no receive and real-time processing resources are assigned to the subarray\n",
    "\n",
    "Going to set the device into its operational state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.On()\n",
    "d.state()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The state should now be in `ON`. This means it has now been transitioned to operational state.\n",
    "\n",
    "Now, we need to start the execution block using the `AssignResources` command. It takes an argument which\n",
    "contains configuration data in JSON format. The data are described by a schema which is versioned to support\n",
    "evolution of the interfaces. The schema is specified in the argument with the interface keyword. More details about the\n",
    "schemas can be found [here](https://developer.skao.int/projects/ska-telmodel/en/latest/).\n",
    "\n",
    "The configuration string defines externally managed resources, an execution block (EB) and processing blocks (PBs). PBs are required for SDP to receive visibility data from the\n",
    "correlator beam-former (CBF), provide calibration solutions, receive candidate and timing data from the pulsar search\n",
    "and timing subsystems and defines scan types.\n",
    "\n",
    "Below shows the configuration string for the execution block and two processing blocks. This configuration string runs one realtime (test_receive_addresses)\n",
    "and one batch processing (test_dask) processing scripts.\n",
    "\n",
    "The `test-receive-addresses` script is designed to test the mechanism for generating SDP receive addresses from the channel\n",
    "link map for each scan type which is contained in the list of scan types in the EB.\n",
    "This address map gets published to the appropriate subarray attribute once the SDP subarray finishes the transition following AssignResources.\n",
    "\n",
    "The `test-dask` script is designed to test deploying two instances of a Dask execution engine and executing a simple function on each one.\n",
    "\n",
    "More details about other processing scripts can be found\n",
    "[here](https://developer.skao.int/projects/ska-sdp-script/en/latest/).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import json\n",
    "import random\n",
    "from datetime import date\n",
    "\n",
    "generator = \"notebook\"\n",
    "today = date.today().strftime(\"%Y%m%d\")\n",
    "number = random.randint(0, 99998)\n",
    "\n",
    "EXECUTION_BLOCK_ID = f\"eb-{generator}-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_REALTIME = f\"pb-{generator}-{today}-{number:05d}\"\n",
    "PROCESSING_BLOCK_ID_BATCH = f\"pb-{generator}-{today}-{number+1:05d}\"\n",
    "\n",
    "\n",
    "config = {\n",
    "  \"interface\": \"https://schema.skao.int/ska-sdp-assignres/1.0\",\n",
    "  \"resources\": {\n",
    "    \"receptors\": [\"SKA001\", \"SKA002\", \"SKA003\", \"SKA004\"]\n",
    "  },\n",
    "  \"execution_block\": {\n",
    "    \"eb_id\":f\"{EXECUTION_BLOCK_ID}\",\n",
    "    \"context\": {},\n",
    "    \"max_length\": 3600.0,\n",
    "    \"beams\": [\n",
    "      {\n",
    "       \"beam_id\": \"vis0\", \n",
    "       \"function\": \"visibilities\",\n",
    "       \"visibility_beam_id\": 2\n",
    "      }\n",
    "    ],\n",
    "    \"scan_types\": [\n",
    "      {\n",
    "        \"scan_type_id\": \".default\",\n",
    "        \"beams\": {\n",
    "          \"vis0\": {\n",
    "            \"channels_id\": \"vis_channels\",\n",
    "            \"polarisations_id\": \"all\"\n",
    "          }\n",
    "        }\n",
    "      },\n",
    "      {\n",
    "        \"scan_type_id\": \"target:a\",\n",
    "        \"derive_from\": \".default\",\n",
    "        \"beams\": {\n",
    "          \"vis0\": {\n",
    "            \"field_id\": \"field_a\"\n",
    "          }\n",
    "        }\n",
    "      }\n",
    "    ],\n",
    "    \"channels\": [\n",
    "      {\n",
    "        \"channels_id\": \"vis_channels\",\n",
    "        \"spectral_windows\": [\n",
    "          {\n",
    "            \"spectral_window_id\":\"fsp_1_channels\",\n",
    "            \"count\": 4,\n",
    "            \"start\": 0,\n",
    "            \"stride\": 2,\n",
    "            \"freq_min\": 350000000.0,\n",
    "            \"freq_max\": 368000000.0,\n",
    "            \"link_map\": [[0, 0], [200, 1], [744, 2], [944, 3]]\n",
    "          }\n",
    "        ]\n",
    "      }\n",
    "    ],\n",
    "    \"polarisations\": [\n",
    "      {\n",
    "        \"polarisations_id\": \"all\",\n",
    "        \"corr_type\": [\"XX\", \"XY\", \"YX\", \"YY\"]\n",
    "      }\n",
    "    ],\n",
    "    \"fields\": [\n",
    "      {\n",
    "        \"field_id\": \"field_a\",\n",
    "        \"phase_dir\": {\n",
    "          \"target_name\": \"target_a\",\n",
    "          \"reference_frame\": \"icrs\",\n",
    "          \"attrs\": {\n",
    "            \"c1\": 123.0,\n",
    "            \"c2\": -60.0,\n",
    "            \"epoch\": 2000.0\n",
    "          }\n",
    "        },\n",
    "        \"pointing_fqdn\": \"...\"\n",
    "      }\n",
    "    ]\n",
    "  },\n",
    "  \"processing_blocks\": [\n",
    "    {\n",
    "      \"pb_id\": f\"{PROCESSING_BLOCK_ID_REALTIME}\",\n",
    "      \"script\": {\"kind\": \"realtime\", \"name\": \"test-receive-addresses\", \"version\": \"1.0.0\"},\n",
    "      \"parameters\": {}\n",
    "    },\n",
    "    {\n",
    "      \"pb_id\": f\"{PROCESSING_BLOCK_ID_BATCH}\",\n",
    "      \"script\": {\"kind\": \"batch\", \"name\": \"test-dask\", \"version\": \"1.0.0\"},\n",
    "      \"parameters\": {},\n",
    "      \"dependencies\": [\n",
    "        {\"pb_id\": f\"{PROCESSING_BLOCK_ID_REALTIME}\", \"kind\": [\"calibration\"]}\n",
    "      ]\n",
    "    }\n",
    "  ]\n",
    "}\n",
    "\n",
    "config_eb = json.dumps(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `AssignResources` command assigns resources to the subarray, creates the execution block and the processing blocks\n",
    "and sets receive addresses.\n",
    "\n",
    "The `obsState` will be in `RESOURCING` when the resources are being assigned and then will be set to `IDLE`\n",
    "once the receive and real-time processing resources are assigned to the subarray as specified in the EB."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.AssignResources(config_eb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Once the command above is executed, you need to wait until the `obsState` is set to `IDLE`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "wait_for_obs_state(d, d.obsState.IDLE, timeout=120)\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's check if the `receiveAddresses` attribute is set with a JSON-formatted string which contains host addresses\n",
    "and ports which the CSP subsystems use to send the data. It will also contain names of SDP attributes that provide\n",
    "real-time calibration information to the rest of the system.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.receiveAddresses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "`resources` attribute can be used to check if externally managed resources have been set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.resources"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `Configure` command configures scan type for the following scans. The argument it takes specifies the scan type.\n",
    "It can declare new scan types to add the ones already defined for the execution block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.Configure('{\"interface\": \"https://schema.skao.int/ska-sdp-configure/1.0\", \"scan_type\": \"target:a\"}')\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `obsState` will transition from `CONFIGURING` to `READY` the subarray is ready to scan, i.e. all (if any) execution engines are also up and running.\n",
    "\n",
    "The `Scan` command begins a scan of the configured type. The argument it takes specifies the scan ID."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.Scan('{\"interface\": \"https://schema.skao.int/ska-sdp-scan/1.0\", \"scan_id\": 1}')\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `obsState` will be set to `SCANNING` and it begins scanning.\n",
    "\n",
    "To end the scan, run the following command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.EndScan()\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `obsState` will be set to `READY`.\n",
    "\n",
    "To end the execution block and the real-time processing blocks in the subarray run the following command.\n",
    "This ends the real-time processing blocks and batch processing starts once when the resources are available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.End()\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The `obsState` should be set to `IDLE`.\n",
    "\n",
    "The batch processing script will run until it's carried out the sequence of actions and will finish automatically.\n",
    "\n",
    "To release externally managed resources from the subarray run the following command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.ReleaseResources('{ \"interface\": \"https://schema.skao.int/ska-sdp-releaseres/1.0\", '\n",
    "                   '\"resources\": {\"receptors\": [\"SKA001\", \"SKA002\", \"SKA003\", \"SKA004\"]}}')\n",
    "d.obsState"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "In this case the `obsState` will be transitioning from `RESOURCING` to `IDLE` or `EMPTY`. It will transition to `IDLE`\n",
    "when there are some resources still assigned (for example, four receptors assigned and only two released).\n",
    "\n",
    "To release all the externally managed resources assigned to the subarray, run the `ReleaseAllResources` command.\n",
    "\n",
    "To set the device to inactive state (or turn the device off), run the following command"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "d.Off()\n",
    "d.state()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The state should be set to `OFF`.\n",
    "\n",
    "More details about each of the SDP Subarray commands can be found in the [LMC documentation](https://developer.skao.int/projects/ska-sdp-lmc/en/latest/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
