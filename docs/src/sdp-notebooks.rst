
SDP Jupyter Notebooks
=====================

ska-sdp CLI notebook
--------------------

The source notebook is called `ska-sdp-cli-tutorial.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-cli-tutorial.ipynb>`_.
It will teach you the basics of using the command line interface called ``ska-sdp``,
which was developed to allow developers to directly interact with the
:doc:`Configuration Database <ska-sdp-integration:design/components>`,
and control SDP from there. ``ska-sdp`` is directly available from the
:doc:`SDP Console pod <ska-sdp-integration:design/components>` as well.


PyTango notebook
----------------

The source notebook is called `ska-sdp-tango-tutorial.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-tango-tutorial.ipynb>`_.
It takes you through the steps of how to connect to an SDP subarray device,
how to configure it, run a scan, etc. If you are familiar with using SDP
through the itango console, the steps in this notebook will not be new for you.


ska-sdp vis-receive low notebook
--------------------------------

The source notebook is called `ska-sdp-vis-receive-low-example.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-vis-receive-low-example.ipynb.ipynb>`_.
It will take you through the steps of how to deploy a vis-receive processing script
connected to an SDP subarray device, run a processor, and emulate the sending of LOW data
using the CBF Emulator.


Executing pointing calibration
------------------------------

The source notebook for executing a pointing calibration is called
`ska-sdp-pointing-script.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-pointing-script.ipynb>`_.
It takes you through the steps of running the pointing-offset processing script, which starts the execution
engine running the pointing offset calibration pipeline. Once the pipeline has run,
`ska-sdp-find-pointing-outputs.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-find-pointing-outputs.ipynb>`_
shows how to find and access the output files generated.

Simulation of Global Pointing Observation Load
----------------------------------------------

The notebook for simulating the load of a global pointing observation is called
`ska-sdp-pointing-script-multiple-scans.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-pointing-script-multiple-scans.ipynb>`_.
It is a variation of the pointing calibration notebook with some modifications which allow you to run the
pipeline multiple times using the same data. It is currently set up to run 50 observations of 5 scans each,
but this is configurable. The purpose of this is to simulate a global pointing observation,
which would involve making observations across many different sources. After the observations are complete, the
notebook takes you through how to view the resulting data products. If you are interested in processing time,
the notebook uses a branch of the pointing pipeline which logs the total processing time and time per observation.

Generating pointing calibration data products for global pointing model fitting
-------------------------------------------------------------------------------

The source notebook for generating simulated pointing calibration data products for global pointing model fitting is called
`ska-sdp-test-mock-data.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-test-mock-data.ipynb>`_.
It takes you through the steps of running the test-mock-data-script, which starts the execution engine and generates pointing HDF5
files for fifty sources (assumed to be an observation of fifty sources across the observable sky). Once the pipeline has run,
`ska-sdp-find-pointing-outputs.ipynb <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-find-pointing-outputs.ipynb>`_
shows how to find and access the output files generated.
